USE [master]
GO
/****** Object:  Database [QLRapChieuPhim]    Script Date: 17/06/2017 08:53:42 ******/
CREATE DATABASE [QLRapChieuPhim]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'QLRapChieuPhim', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\QLRapChieuPhim.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'QLRapChieuPhim_log', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\QLRapChieuPhim_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [QLRapChieuPhim] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QLRapChieuPhim].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QLRapChieuPhim] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QLRapChieuPhim] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QLRapChieuPhim] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QLRapChieuPhim] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QLRapChieuPhim] SET ARITHABORT OFF 
GO
ALTER DATABASE [QLRapChieuPhim] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [QLRapChieuPhim] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QLRapChieuPhim] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QLRapChieuPhim] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QLRapChieuPhim] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QLRapChieuPhim] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QLRapChieuPhim] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QLRapChieuPhim] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QLRapChieuPhim] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QLRapChieuPhim] SET  DISABLE_BROKER 
GO
ALTER DATABASE [QLRapChieuPhim] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QLRapChieuPhim] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QLRapChieuPhim] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QLRapChieuPhim] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QLRapChieuPhim] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QLRapChieuPhim] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QLRapChieuPhim] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QLRapChieuPhim] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [QLRapChieuPhim] SET  MULTI_USER 
GO
ALTER DATABASE [QLRapChieuPhim] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QLRapChieuPhim] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QLRapChieuPhim] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QLRapChieuPhim] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [QLRapChieuPhim] SET DELAYED_DURABILITY = DISABLED 
GO
USE [QLRapChieuPhim]
GO
/****** Object:  Table [dbo].[DinhDang]    Script Date: 17/06/2017 08:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DinhDang](
	[MaDinhDang] [char](10) NOT NULL,
	[TenDinhDang] [nvarchar](50) NULL,
 CONSTRAINT [PK_DinhDang] PRIMARY KEY CLUSTERED 
(
	[MaDinhDang] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DinhDang_Phim]    Script Date: 17/06/2017 08:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DinhDang_Phim](
	[MaPhim] [char](10) NOT NULL,
	[MaDinhDang] [char](10) NOT NULL,
 CONSTRAINT [PK_DinhDang_Phim] PRIMARY KEY CLUSTERED 
(
	[MaPhim] ASC,
	[MaDinhDang] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KhachHang]    Script Date: 17/06/2017 08:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KhachHang](
	[SDT] [char](12) NOT NULL,
	[TongTien] [decimal](18, 0) NULL,
 CONSTRAINT [PK_KhachHang] PRIMARY KEY CLUSTERED 
(
	[SDT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LichChieu]    Script Date: 17/06/2017 08:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LichChieu](
	[Ngay] [datetime] NOT NULL,
	[GiamGia] [float] NULL,
 CONSTRAINT [PK_LichChieu] PRIMARY KEY CLUSTERED 
(
	[Ngay] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiNV]    Script Date: 17/06/2017 08:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiNV](
	[MaLoaiNV] [char](10) NOT NULL,
	[VaiTro] [nvarchar](50) NULL,
 CONSTRAINT [PK_LoaiNV] PRIMARY KEY CLUSTERED 
(
	[MaLoaiNV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiPhim]    Script Date: 17/06/2017 08:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiPhim](
	[MaLoai] [char](10) NOT NULL,
	[TenTheLoai] [nvarchar](50) NULL,
	[BiXoa] [bit] NULL,
 CONSTRAINT [PK_LoaiPhim] PRIMARY KEY CLUSTERED 
(
	[MaLoai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiTaiKhoan]    Script Date: 17/06/2017 08:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiTaiKhoan](
	[MaLoaiTaiKhoan] [char](10) NOT NULL,
	[TenLoaiTaiKhoan] [nvarchar](50) NULL,
 CONSTRAINT [PK_LoaiTaiKhoan] PRIMARY KEY CLUSTERED 
(
	[MaLoaiTaiKhoan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiVe]    Script Date: 17/06/2017 08:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiVe](
	[MaLoaiVe] [char](10) NOT NULL,
	[TenLoaiVe] [nvarchar](50) NOT NULL,
	[LoaiChoNgoi] [nvarchar](50) NULL,
	[LoaiKhachHang] [nvarchar](50) NULL,
	[MaDinhDang] [char](10) NULL,
	[GiaVe] [float] NULL,
	[BiXoa] [bit] NULL,
 CONSTRAINT [PK_LoaiVe] PRIMARY KEY CLUSTERED 
(
	[MaLoaiVe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NhaCCPhim]    Script Date: 17/06/2017 08:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NhaCCPhim](
	[MaNhaCC] [char](10) NOT NULL,
	[TenNhaCC] [nvarchar](50) NULL,
	[BiXoa] [bit] NULL,
 CONSTRAINT [PK_NhaCCPhim] PRIMARY KEY CLUSTERED 
(
	[MaNhaCC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NhanVien]    Script Date: 17/06/2017 08:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NhanVien](
	[MaNV] [char](10) NOT NULL,
	[TenNV] [varchar](50) NULL,
	[MaLoaiNV] [char](10) NOT NULL,
	[BiXoa] [bit] NULL,
 CONSTRAINT [PK_NhanVien] PRIMARY KEY CLUSTERED 
(
	[MaNV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Phim]    Script Date: 17/06/2017 08:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Phim](
	[MaPhim] [char](10) NOT NULL,
	[TenPhim] [nvarchar](50) NULL,
	[TheLoai] [char](10) NULL,
	[NhaCungCap] [char](10) NULL,
	[NhanVienQL] [char](10) NULL,
	[BiXoa] [bit] NULL,
	[MieuTa] [nvarchar](50) NULL,
	[PosterFilm] [varchar](255) NULL,
 CONSTRAINT [PK_Phim] PRIMARY KEY CLUSTERED 
(
	[MaPhim] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PhongChieu]    Script Date: 17/06/2017 08:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PhongChieu](
	[MaPChieu] [char](10) NOT NULL,
	[TenPChieu] [nvarchar](50) NULL,
	[BiXoa] [bit] NULL,
 CONSTRAINT [PK_PhongChieu] PRIMARY KEY CLUSTERED 
(
	[MaPChieu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SuatChieu]    Script Date: 17/06/2017 08:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SuatChieu](
	[Ngay] [datetime] NOT NULL,
	[Suat] [int] IDENTITY(1,1) NOT NULL,
	[MaPhim] [char](10) NULL,
	[MaPChieu] [char](10) NULL,
	[ThoiGian] [varchar](50) NULL,
	[BiXoa] [bit] NULL CONSTRAINT [DF_SuatChieu_BiXoa]  DEFAULT ((0)),
 CONSTRAINT [PK_SuatChieu] PRIMARY KEY CLUSTERED 
(
	[Ngay] ASC,
	[Suat] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TaiKhoan]    Script Date: 17/06/2017 08:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TaiKhoan](
	[MaTaiKhoan] [char](10) NOT NULL,
	[TenDangNhap] [nchar](20) NULL,
	[MatKhau] [varchar](20) NULL,
	[BiXoa] [bit] NULL,
	[MaLoaiTaiKhoan] [char](10) NULL,
	[MaNV] [char](10) NULL,
 CONSTRAINT [PK_TaiKhoan] PRIMARY KEY CLUSTERED 
(
	[MaTaiKhoan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ve]    Script Date: 17/06/2017 08:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ve](
	[MaVe] [char](10) NOT NULL,
	[LoaiVe] [char](10) NULL,
	[Ngay] [datetime] NULL,
	[Suat] [int] NULL,
	[SDT] [char](12) NULL,
	[TenKH] [nvarchar](50) NULL,
	[NVBanVe] [char](10) NULL,
	[BiXoa] [bit] NULL,
 CONSTRAINT [PK_Ve] PRIMARY KEY CLUSTERED 
(
	[MaVe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[DinhDang] ([MaDinhDang], [TenDinhDang]) VALUES (N'DD001     ', N'2D')
INSERT [dbo].[DinhDang] ([MaDinhDang], [TenDinhDang]) VALUES (N'DD002     ', N'3D')
INSERT [dbo].[DinhDang] ([MaDinhDang], [TenDinhDang]) VALUES (N'DD003     ', N'4DMax')
INSERT [dbo].[KhachHang] ([SDT], [TongTien]) VALUES (N'01213       ', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[KhachHang] ([SDT], [TongTien]) VALUES (N'01215946244 ', CAST(200000 AS Decimal(18, 0)))
INSERT [dbo].[KhachHang] ([SDT], [TongTien]) VALUES (N'012334433   ', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[KhachHang] ([SDT], [TongTien]) VALUES (N'020121      ', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[KhachHang] ([SDT], [TongTien]) VALUES (N'0210212     ', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[KhachHang] ([SDT], [TongTien]) VALUES (N'0913453533  ', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[LichChieu] ([Ngay], [GiamGia]) VALUES (CAST(N'2017-02-05 00:00:00.000' AS DateTime), NULL)
INSERT [dbo].[LichChieu] ([Ngay], [GiamGia]) VALUES (CAST(N'2017-03-05 00:00:00.000' AS DateTime), NULL)
INSERT [dbo].[LichChieu] ([Ngay], [GiamGia]) VALUES (CAST(N'2017-04-26 00:00:00.000' AS DateTime), 0)
INSERT [dbo].[LichChieu] ([Ngay], [GiamGia]) VALUES (CAST(N'2017-04-28 00:00:00.000' AS DateTime), 3)
INSERT [dbo].[LichChieu] ([Ngay], [GiamGia]) VALUES (CAST(N'2017-05-18 00:00:00.000' AS DateTime), 40)
INSERT [dbo].[LichChieu] ([Ngay], [GiamGia]) VALUES (CAST(N'2017-05-19 00:00:00.000' AS DateTime), 40)
INSERT [dbo].[LichChieu] ([Ngay], [GiamGia]) VALUES (CAST(N'2017-06-02 00:00:00.000' AS DateTime), 20)
INSERT [dbo].[LichChieu] ([Ngay], [GiamGia]) VALUES (CAST(N'2017-06-04 00:00:00.000' AS DateTime), 0)
INSERT [dbo].[LichChieu] ([Ngay], [GiamGia]) VALUES (CAST(N'2017-06-08 00:00:00.000' AS DateTime), 0)
INSERT [dbo].[LichChieu] ([Ngay], [GiamGia]) VALUES (CAST(N'2017-06-16 00:00:00.000' AS DateTime), 44)
INSERT [dbo].[LichChieu] ([Ngay], [GiamGia]) VALUES (CAST(N'2017-06-17 00:00:00.000' AS DateTime), 55)
INSERT [dbo].[LichChieu] ([Ngay], [GiamGia]) VALUES (CAST(N'2017-06-24 00:00:00.000' AS DateTime), 40)
INSERT [dbo].[LichChieu] ([Ngay], [GiamGia]) VALUES (CAST(N'2017-06-25 00:00:00.000' AS DateTime), 32)
INSERT [dbo].[LichChieu] ([Ngay], [GiamGia]) VALUES (CAST(N'2017-07-13 00:00:00.000' AS DateTime), 0)
INSERT [dbo].[LichChieu] ([Ngay], [GiamGia]) VALUES (CAST(N'2017-08-06 00:00:00.000' AS DateTime), 0)
INSERT [dbo].[LoaiNV] ([MaLoaiNV], [VaiTro]) VALUES (N'LNV001    ', N'Quan Ly Phim')
INSERT [dbo].[LoaiPhim] ([MaLoai], [TenTheLoai], [BiXoa]) VALUES (N'LP001     ', N'Khoa hoc vien tuong', NULL)
INSERT [dbo].[LoaiVe] ([MaLoaiVe], [TenLoaiVe], [LoaiChoNgoi], [LoaiKhachHang], [MaDinhDang], [GiaVe], [BiXoa]) VALUES (N'LV0       ', N'Vé thường', N'A19', N'Thường', N'DD003     ', 90000, 0)
INSERT [dbo].[LoaiVe] ([MaLoaiVe], [TenLoaiVe], [LoaiChoNgoi], [LoaiKhachHang], [MaDinhDang], [GiaVe], [BiXoa]) VALUES (N'LV1       ', N'Vé học sinh, sinh viên', N'A20', N'Học sinh, sinh viên', N'DD001     ', 30000, 0)
INSERT [dbo].[LoaiVe] ([MaLoaiVe], [TenLoaiVe], [LoaiChoNgoi], [LoaiKhachHang], [MaDinhDang], [GiaVe], [BiXoa]) VALUES (N'LV2       ', N'Tặng', N'A1', N'Thân thiết', N'DD003     ', 0, 0)
INSERT [dbo].[LoaiVe] ([MaLoaiVe], [TenLoaiVe], [LoaiChoNgoi], [LoaiKhachHang], [MaDinhDang], [GiaVe], [BiXoa]) VALUES (N'LV3       ', N'Vé V.I.P', N'B1', N'Khách vip', N'DD002     ', 200000, 0)
INSERT [dbo].[NhaCCPhim] ([MaNhaCC], [TenNhaCC], [BiXoa]) VALUES (N'CC001     ', N'Gary Ross', NULL)
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [MaLoaiNV], [BiXoa]) VALUES (N'NV001     ', N'Nguyen Toan Phat', N'LNV001    ', 0)
INSERT [dbo].[Phim] ([MaPhim], [TenPhim], [TheLoai], [NhaCungCap], [NhanVienQL], [BiXoa], [MieuTa], [PosterFilm]) VALUES (N'P001      ', N'The Hunger Game', N'LP001     ', N'CC001     ', N'NV001     ', 0, N'Phim hay', N'hunger-games-catching-fire-movie-poster.jpg')
INSERT [dbo].[Phim] ([MaPhim], [TenPhim], [TheLoai], [NhaCungCap], [NhanVienQL], [BiXoa], [MieuTa], [PosterFilm]) VALUES (N'P002      ', N'Harry Potter', N'LP001     ', N'CC001     ', N'NV001     ', 0, N'Phim hay', N'hary_film.jpg')
INSERT [dbo].[Phim] ([MaPhim], [TenPhim], [TheLoai], [NhaCungCap], [NhanVienQL], [BiXoa], [MieuTa], [PosterFilm]) VALUES (N'P003      ', N'Thor', N'LP001     ', N'CC001     ', N'NV001     ', 0, N'Phim hay', N'Thor-Film-Poster.jpg')
INSERT [dbo].[Phim] ([MaPhim], [TenPhim], [TheLoai], [NhaCungCap], [NhanVienQL], [BiXoa], [MieuTa], [PosterFilm]) VALUES (N'P004      ', N'Em chưa 18', N'LP001     ', N'CC001     ', N'NV001     ', 0, N'Phim Viet', N'em-chua-18-1493288295537.jpg')
INSERT [dbo].[Phim] ([MaPhim], [TenPhim], [TheLoai], [NhaCungCap], [NhanVienQL], [BiXoa], [MieuTa], [PosterFilm]) VALUES (N'P005      ', N'Nhóc Trùm', N'LP001     ', N'CC001     ', N'NV001     ', 0, N'Phim Việt', N'img20161101074616000.jpg')
INSERT [dbo].[Phim] ([MaPhim], [TenPhim], [TheLoai], [NhaCungCap], [NhanVienQL], [BiXoa], [MieuTa], [PosterFilm]) VALUES (N'P006      ', N'Fast & Furious 8', N'LP001     ', NULL, NULL, 0, NULL, N'a2d58fa64_1492229723.jpg')
INSERT [dbo].[PhongChieu] ([MaPChieu], [TenPChieu], [BiXoa]) VALUES (N'PC001     ', N'Phong chieu A', NULL)
INSERT [dbo].[PhongChieu] ([MaPChieu], [TenPChieu], [BiXoa]) VALUES (N'PC002     ', N'Phong chieu B', NULL)
SET IDENTITY_INSERT [dbo].[SuatChieu] ON 

INSERT [dbo].[SuatChieu] ([Ngay], [Suat], [MaPhim], [MaPChieu], [ThoiGian], [BiXoa]) VALUES (CAST(N'2017-02-05 00:00:00.000' AS DateTime), 2, NULL, NULL, NULL, 1)
INSERT [dbo].[SuatChieu] ([Ngay], [Suat], [MaPhim], [MaPChieu], [ThoiGian], [BiXoa]) VALUES (CAST(N'2017-02-05 00:00:00.000' AS DateTime), 14, N'P003      ', N'PC002     ', N'10:10', 0)
INSERT [dbo].[SuatChieu] ([Ngay], [Suat], [MaPhim], [MaPChieu], [ThoiGian], [BiXoa]) VALUES (CAST(N'2017-03-05 00:00:00.000' AS DateTime), 2, NULL, NULL, NULL, 1)
INSERT [dbo].[SuatChieu] ([Ngay], [Suat], [MaPhim], [MaPChieu], [ThoiGian], [BiXoa]) VALUES (CAST(N'2017-03-05 00:00:00.000' AS DateTime), 3, NULL, NULL, NULL, 1)
INSERT [dbo].[SuatChieu] ([Ngay], [Suat], [MaPhim], [MaPChieu], [ThoiGian], [BiXoa]) VALUES (CAST(N'2017-04-28 00:00:00.000' AS DateTime), 4, N'P002      ', N'PC001     ', N'12:30', 1)
INSERT [dbo].[SuatChieu] ([Ngay], [Suat], [MaPhim], [MaPChieu], [ThoiGian], [BiXoa]) VALUES (CAST(N'2017-04-28 00:00:00.000' AS DateTime), 5, N'P003      ', N'PC002     ', N'12:30', 0)
INSERT [dbo].[SuatChieu] ([Ngay], [Suat], [MaPhim], [MaPChieu], [ThoiGian], [BiXoa]) VALUES (CAST(N'2017-05-18 00:00:00.000' AS DateTime), 6, N'P003      ', N'PC002     ', N'12:40', 1)
INSERT [dbo].[SuatChieu] ([Ngay], [Suat], [MaPhim], [MaPChieu], [ThoiGian], [BiXoa]) VALUES (CAST(N'2017-05-18 00:00:00.000' AS DateTime), 13, N'P004      ', N'PC002     ', N'5:50', 0)
INSERT [dbo].[SuatChieu] ([Ngay], [Suat], [MaPhim], [MaPChieu], [ThoiGian], [BiXoa]) VALUES (CAST(N'2017-05-19 00:00:00.000' AS DateTime), 11, N'P003      ', N'PC002     ', N'16:30', NULL)
INSERT [dbo].[SuatChieu] ([Ngay], [Suat], [MaPhim], [MaPChieu], [ThoiGian], [BiXoa]) VALUES (CAST(N'2017-06-02 00:00:00.000' AS DateTime), 7, N'P001      ', N'PC002     ', N'12:10', 0)
INSERT [dbo].[SuatChieu] ([Ngay], [Suat], [MaPhim], [MaPChieu], [ThoiGian], [BiXoa]) VALUES (CAST(N'2017-06-02 00:00:00.000' AS DateTime), 9, N'P003      ', N'PC002     ', N'11:50', 0)
INSERT [dbo].[SuatChieu] ([Ngay], [Suat], [MaPhim], [MaPChieu], [ThoiGian], [BiXoa]) VALUES (CAST(N'2017-06-02 00:00:00.000' AS DateTime), 10, N'P003      ', N'PC002     ', N'11:50', 0)
INSERT [dbo].[SuatChieu] ([Ngay], [Suat], [MaPhim], [MaPChieu], [ThoiGian], [BiXoa]) VALUES (CAST(N'2017-06-02 00:00:00.000' AS DateTime), 12, N'P001      ', N'PC002     ', N'17:50', NULL)
INSERT [dbo].[SuatChieu] ([Ngay], [Suat], [MaPhim], [MaPChieu], [ThoiGian], [BiXoa]) VALUES (CAST(N'2017-06-08 00:00:00.000' AS DateTime), 8, N'P001      ', N'PC002     ', N'12:30', 0)
INSERT [dbo].[SuatChieu] ([Ngay], [Suat], [MaPhim], [MaPChieu], [ThoiGian], [BiXoa]) VALUES (CAST(N'2017-07-13 00:00:00.000' AS DateTime), 15, N'P003      ', N'PC002     ', N'21:10', 1)
SET IDENTITY_INSERT [dbo].[SuatChieu] OFF
INSERT [dbo].[Ve] ([MaVe], [LoaiVe], [Ngay], [Suat], [SDT], [TenKH], [NVBanVe], [BiXoa]) VALUES (N'V1        ', N'LV1       ', CAST(N'2017-06-02 00:00:00.000' AS DateTime), 9, N'0913453533  ', N'Nguyen Van A', NULL, 1)
INSERT [dbo].[Ve] ([MaVe], [LoaiVe], [Ngay], [Suat], [SDT], [TenKH], [NVBanVe], [BiXoa]) VALUES (N'V2        ', N'LV0       ', CAST(N'2017-06-02 00:00:00.000' AS DateTime), 9, NULL, N'Nguyen Van B', NULL, 0)
INSERT [dbo].[Ve] ([MaVe], [LoaiVe], [Ngay], [Suat], [SDT], [TenKH], [NVBanVe], [BiXoa]) VALUES (N'V3        ', N'LV1       ', CAST(N'2017-06-02 00:00:00.000' AS DateTime), 12, N'01213       ', N'Nguyen Van Điền', NULL, 0)
INSERT [dbo].[Ve] ([MaVe], [LoaiVe], [Ngay], [Suat], [SDT], [TenKH], [NVBanVe], [BiXoa]) VALUES (N'V4        ', N'LV0       ', CAST(N'2017-04-28 00:00:00.000' AS DateTime), 4, N'01215946244 ', N'Nguyen Van D', NULL, 1)
INSERT [dbo].[Ve] ([MaVe], [LoaiVe], [Ngay], [Suat], [SDT], [TenKH], [NVBanVe], [BiXoa]) VALUES (N'V5        ', N'LV2       ', CAST(N'2017-04-28 00:00:00.000' AS DateTime), 4, N'0210212     ', N'Nguyễn Văn Hunger', NULL, 0)
ALTER TABLE [dbo].[DinhDang_Phim]  WITH CHECK ADD  CONSTRAINT [FK_DinhDang_Phim_DinhDang] FOREIGN KEY([MaDinhDang])
REFERENCES [dbo].[DinhDang] ([MaDinhDang])
GO
ALTER TABLE [dbo].[DinhDang_Phim] CHECK CONSTRAINT [FK_DinhDang_Phim_DinhDang]
GO
ALTER TABLE [dbo].[DinhDang_Phim]  WITH CHECK ADD  CONSTRAINT [FK_DinhDang_Phim_Phim] FOREIGN KEY([MaPhim])
REFERENCES [dbo].[Phim] ([MaPhim])
GO
ALTER TABLE [dbo].[DinhDang_Phim] CHECK CONSTRAINT [FK_DinhDang_Phim_Phim]
GO
ALTER TABLE [dbo].[LoaiVe]  WITH CHECK ADD  CONSTRAINT [FK_LoaiVe_DinhDang] FOREIGN KEY([MaDinhDang])
REFERENCES [dbo].[DinhDang] ([MaDinhDang])
GO
ALTER TABLE [dbo].[LoaiVe] CHECK CONSTRAINT [FK_LoaiVe_DinhDang]
GO
ALTER TABLE [dbo].[NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_NhanVien_LoaiNV] FOREIGN KEY([MaLoaiNV])
REFERENCES [dbo].[LoaiNV] ([MaLoaiNV])
GO
ALTER TABLE [dbo].[NhanVien] CHECK CONSTRAINT [FK_NhanVien_LoaiNV]
GO
ALTER TABLE [dbo].[Phim]  WITH CHECK ADD  CONSTRAINT [FK_Phim_LoaiPhim] FOREIGN KEY([TheLoai])
REFERENCES [dbo].[LoaiPhim] ([MaLoai])
GO
ALTER TABLE [dbo].[Phim] CHECK CONSTRAINT [FK_Phim_LoaiPhim]
GO
ALTER TABLE [dbo].[Phim]  WITH CHECK ADD  CONSTRAINT [FK_Phim_NhaCCPhim] FOREIGN KEY([NhaCungCap])
REFERENCES [dbo].[NhaCCPhim] ([MaNhaCC])
GO
ALTER TABLE [dbo].[Phim] CHECK CONSTRAINT [FK_Phim_NhaCCPhim]
GO
ALTER TABLE [dbo].[Phim]  WITH CHECK ADD  CONSTRAINT [FK_Phim_NhanVien] FOREIGN KEY([NhanVienQL])
REFERENCES [dbo].[NhanVien] ([MaNV])
GO
ALTER TABLE [dbo].[Phim] CHECK CONSTRAINT [FK_Phim_NhanVien]
GO
ALTER TABLE [dbo].[SuatChieu]  WITH CHECK ADD  CONSTRAINT [FK_SuatChieu_LichChieu] FOREIGN KEY([Ngay])
REFERENCES [dbo].[LichChieu] ([Ngay])
GO
ALTER TABLE [dbo].[SuatChieu] CHECK CONSTRAINT [FK_SuatChieu_LichChieu]
GO
ALTER TABLE [dbo].[SuatChieu]  WITH CHECK ADD  CONSTRAINT [FK_SuatChieu_Phim] FOREIGN KEY([MaPhim])
REFERENCES [dbo].[Phim] ([MaPhim])
GO
ALTER TABLE [dbo].[SuatChieu] CHECK CONSTRAINT [FK_SuatChieu_Phim]
GO
ALTER TABLE [dbo].[SuatChieu]  WITH CHECK ADD  CONSTRAINT [FK_SuatChieu_PhongChieu] FOREIGN KEY([MaPChieu])
REFERENCES [dbo].[PhongChieu] ([MaPChieu])
GO
ALTER TABLE [dbo].[SuatChieu] CHECK CONSTRAINT [FK_SuatChieu_PhongChieu]
GO
ALTER TABLE [dbo].[TaiKhoan]  WITH CHECK ADD  CONSTRAINT [FK_TaiKhoan_LoaiTaiKhoan] FOREIGN KEY([MaLoaiTaiKhoan])
REFERENCES [dbo].[LoaiTaiKhoan] ([MaLoaiTaiKhoan])
GO
ALTER TABLE [dbo].[TaiKhoan] CHECK CONSTRAINT [FK_TaiKhoan_LoaiTaiKhoan]
GO
ALTER TABLE [dbo].[TaiKhoan]  WITH CHECK ADD  CONSTRAINT [FK_TaiKhoan_NhanVien] FOREIGN KEY([MaNV])
REFERENCES [dbo].[NhanVien] ([MaNV])
GO
ALTER TABLE [dbo].[TaiKhoan] CHECK CONSTRAINT [FK_TaiKhoan_NhanVien]
GO
ALTER TABLE [dbo].[Ve]  WITH CHECK ADD  CONSTRAINT [FK_Ve_KhachHang] FOREIGN KEY([SDT])
REFERENCES [dbo].[KhachHang] ([SDT])
GO
ALTER TABLE [dbo].[Ve] CHECK CONSTRAINT [FK_Ve_KhachHang]
GO
ALTER TABLE [dbo].[Ve]  WITH CHECK ADD  CONSTRAINT [FK_Ve_LoaiVe] FOREIGN KEY([LoaiVe])
REFERENCES [dbo].[LoaiVe] ([MaLoaiVe])
GO
ALTER TABLE [dbo].[Ve] CHECK CONSTRAINT [FK_Ve_LoaiVe]
GO
ALTER TABLE [dbo].[Ve]  WITH CHECK ADD  CONSTRAINT [FK_Ve_NhanVien] FOREIGN KEY([NVBanVe])
REFERENCES [dbo].[NhanVien] ([MaNV])
GO
ALTER TABLE [dbo].[Ve] CHECK CONSTRAINT [FK_Ve_NhanVien]
GO
ALTER TABLE [dbo].[Ve]  WITH CHECK ADD  CONSTRAINT [FK_Ve_SuatChieu] FOREIGN KEY([Ngay], [Suat])
REFERENCES [dbo].[SuatChieu] ([Ngay], [Suat])
GO
ALTER TABLE [dbo].[Ve] CHECK CONSTRAINT [FK_Ve_SuatChieu]
GO
USE [master]
GO
ALTER DATABASE [QLRapChieuPhim] SET  READ_WRITE 
GO
