﻿using System.Windows;
namespace DoAn
{
    /// <summary>
    /// Interaction logic for TaoVe.xaml
    /// </summary>
    public partial class TaoVe : Window
    {
        public TaoVe()
        {
            ViewModel.DataViewModel.PageSize = 5;
            ViewModel.DataViewModel.ManHinhDangChay = 3;
            InitializeComponent();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult mbr = MessageBox.Show("Xác nhận thoát", "Thoát", MessageBoxButton.YesNo);
            if(mbr == MessageBoxResult.No)
            {
                return;
            }
            ViewModel.DataViewModel.ManHinhDangChay = 1;
            ViewModel.DataViewModel.PageSize = 10;
            this.Close();
        }
    }
}
