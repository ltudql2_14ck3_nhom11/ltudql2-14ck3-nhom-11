﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DoAn
{
    /// <summary>
    /// Interaction logic for ManHinhChinh.xaml
    /// </summary>
    public partial class ManHinhChinh : Window
    {
        public ManHinhChinh(string tnv, string mtk)
        {
            ViewModel.DataViewModel.ManHinhDangChay = 9;
            InitializeComponent();
            lbTenNV.Content = tnv;
            maTK = mtk;
            if (tnv != "Admin")
            {
                QLNV.IsEnabled = false;
            }
        }
        string maTK = null;
        private void RibbonButton_Click(object sender, RoutedEventArgs e)
        {
            this.myPlace.Children.Clear();               
            this.myPlace.Children.Add (new QuanLyNhaCC());   
        }

        private void RibbonButton_Click_1(object sender, RoutedEventArgs e)
        {
            this.myPlace.Children.Clear();
            this.myPlace.Children.Add(new QuanLyLoaiPhim());
        }

        private void RibbonButton_Click_2(object sender, RoutedEventArgs e)
        {
            this.myPlace.Children.Clear();
            this.myPlace.Children.Add(new QuanLyPhongChieu());
        }

        private void RibbonButton_Click_3(object sender, RoutedEventArgs e)
        {
            this.myPlace.Children.Clear();
            this.myPlace.Children.Add(new QuanLyPhim());
        }

        private void RibbonButton_Click_4(object sender, RoutedEventArgs e)
        {
            this.myPlace.Children.Clear();
            this.myPlace.Children.Add(new QuanLyNhanVien());
        }

        private void DoiMK_Click(object sender, RoutedEventArgs e)
        {
            DoiMatKhau frm = new DoiMatKhau(maTK);
            this.Hide();
            //this.Close();
            frm.ShowDialog();
            this.ShowDialog();
        }

        private void Thoat_Click(object sender, RoutedEventArgs e)
        {
            DangNhap frm = new DangNhap();
            this.Close();
            frm.ShowDialog();
        }

        private void RibbonButton_Click_5(object sender, RoutedEventArgs e)
        {
            this.myPlace.Children.Clear();
            this.myPlace.Children.Add(new BaoCao());
        }

        private void rBtnMinimize_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void rBtnRestoreDown_Click(object sender, RoutedEventArgs e)
        {
            if (flat == 1)
            {
                WindowState = WindowState.Maximized;
                flat = 2;
            }
            else if (flat == 2)
            {
                flat = 1;
                WindowState = WindowState.Normal;
            }
        }

        private void rBtnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        int flat = 1;

    }
}
