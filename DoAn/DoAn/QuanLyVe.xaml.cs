﻿using DoAn.UControl;
using Model;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace DoAn
{
    /// <summary>
    /// Interaction logic for QuanLyVe.xaml
    /// </summary>
    public partial class QuanLyVe : Window
    {
        string maTK = null;
        public QuanLyVe(string mtk)
        {
            ViewModel.DataViewModel.PageSize = 10;
            ViewModel.DataViewModel.ManHinhDangChay = 1;
            InitializeComponent();
            maTK = mtk;
        }

        private void rbtnKH_Click(object sender, RoutedEventArgs e)
        {
            this.placeKHThanThiet.IsEnabled = !this.rbtnKH.IsChecked.Value;
        }
    
        private void rbtnTT_Click(object sender, RoutedEventArgs e)
        {
            this.placeKHThanThiet.IsEnabled = this.rbtnTT.IsChecked.Value;
        }

        private void btnTaoVeOpen_Click(object sender, RoutedEventArgs e)
        {
            TaoVe form = new TaoVe();
            form.Show();
        }

        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult mbr = MessageBox.Show("Xác nhận thoát", "Thoát", MessageBoxButton.YesNo);
            if (mbr == MessageBoxResult.No)
            {
                return;
            }
            this.Close();
        }

      

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            if (this.lbxHoaDon.ItemsSource == null)
            {
                MessageBox.Show("Tạo vé trước khi in");
                return;
            }
            if(((ObservableCollection<HoaDon>)this.lbxHoaDon.ItemsSource).Count == 0)
            {
                MessageBox.Show("Tạo vé trước khi in");
                return;
            }
            PrintDialog printDialog = new PrintDialog();
            var uc = new HoaDonUC();
            var aa = this.lbxHoaDon.ItemsSource;
            uc.setHoaDon(this.lbxHoaDon.ItemsSource);
            if ((bool)printDialog.ShowDialog().GetValueOrDefault())
            {
                uc.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));

                Size sizeGrid = uc.DesiredSize;
                Point ptGird = new Point(350,
                          300);

                uc.Arrange(new Rect(ptGird, sizeGrid));

                printDialog.PrintVisual(uc, Title);
            }
           ((ObservableCollection<HoaDon>)this.lbxHoaDon.ItemsSource).Clear();
        }

        private void btnDMK_Click(object sender, RoutedEventArgs e)
        {
            DoiMatKhau frm = new DoiMatKhau(maTK);
            this.Hide();
            frm.ShowDialog();
            this.ShowDialog();
        }
    }
}