﻿using System;
using System.Linq;
using System.Windows;
using Model;

namespace DoAn
{
    /// <summary>
    /// Interaction logic for DangNhap.xaml
    /// </summary>
    public partial class DangNhap : Window
    {
        QLRapChieuPhimDBEntities db = new QLRapChieuPhimDBEntities();
        public DangNhap()
        {
            InitializeComponent();
        }

        //Mã hóa md5
        public static byte[] encryptData(string data)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider md5Hasher = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] hashedBytes;
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            hashedBytes = md5Hasher.ComputeHash(encoder.GetBytes(data));
            return hashedBytes;
        }
        public static string md5(string data)
        {
            return BitConverter.ToString(encryptData(data)).Replace("-", "").ToLower();
        }

        private void btnDN_Click(object sender, RoutedEventArgs e)
        {
            string mk = md5(txtPass.Password.ToString());
            //string matkhau = txtPass.Password.ToString();
            var tk = db.TaiKhoans.Where(k => k.TenDangNhap == txtUsername.Text && k.MatKhau == mk && k.BiXoa == false).ToList();
            foreach (var item in tk)
            {
                if(item.NhanVien.MaLoaiNV == "LNV001    ")
                {
                    ManHinhChinh frm = new ManHinhChinh(item.NhanVien.TenNV, item.MaTaiKhoan);
                    this.Hide();
                    this.Close();
                    frm.ShowDialog();
                    //this.Show();
                }
                else if(item.NhanVien.MaLoaiNV == "LNV002    ")
                {
                    QuanLyVe frm = new QuanLyVe(item.MaTaiKhoan);
                    this.Hide();
                    this.Close();
                    frm.ShowDialog();
                }
                else if (item.NhanVien.MaLoaiNV == "LNV003    ")
                {
                    QuanLySuatChieu frm = new QuanLySuatChieu(item.MaTaiKhoan);
                    this.Hide();
                    this.Close();
                    frm.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Đăng nhập thất bại.");
                }
            }
            if(tk.Count() == 0)
            {
                MessageBox.Show("Tên đăng nhập hoặc mật khẩu không đúng.");
            }
        }

        private void btnThoat_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
