﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Model;

namespace DoAn
{
    /// <summary>
    /// Interaction logic for QuanLyNhanVien.xaml
    /// </summary>
    public partial class QuanLyNhanVien : UserControl
    {
        public QuanLyNhanVien()
        {
            ViewModel.DataViewModel.ManHinhDangChay = 4;
            InitializeComponent();          
        }
        //private void Button_Click(object sender, RoutedEventArgs e)
        //{
        //    this.Close();
        //}

        //private void RibbonButton_Click(object sender, RoutedEventArgs e)
        //{
        //    SolidColorBrush myBrush = new SolidColorBrush(Colors.Violet);
        //    Home.Background = myBrush;
        //    RB.Background = myBrush;
        //    Tab1.Background = myBrush;
        //}

        //private void rBtnMinimize_Click(object sender, RoutedEventArgs e)
        //{
        //    WindowState = WindowState.Minimized;
        //}
        //int flat = 1;
        //private void rBtnRestoreDown_Click(object sender, RoutedEventArgs e)
        //{
        //    if (flat == 1)
        //    {
        //        WindowState = WindowState.Maximized;
        //        flat = 2;
        //    }
        //    else if (flat == 2)
        //    {
        //        flat = 1;
        //        WindowState = WindowState.Normal;
        //    }
        //}

        //private void rBtnClose_Click(object sender, RoutedEventArgs e)
        //{
        //    ManHinhChinh frm = new ManHinhChinh();
        //    this.Close();
        //    frm.ShowDialog();
        //}

        private void dgvNV_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int k = 0;
            var nv = dgvNV.SelectedItem as NhanVien;
            if (nv.LoaiNV.VaiTro == "Nhân viên")
                k = 1;
            txtTenNV.Text = nv.TenNV.Trim();
            cbxChucVu.SelectedIndex = k;
            txtSDTNV.Text = nv.SDTNV.Trim();
            txtDiaChiNV.Text = nv.DiaChi.Trim();
        }

        private void dgvTK_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int k = 0;
            var tk = dgvTK.SelectedItem as TaiKhoan;
            if (tk.LoaiTaiKhoan.TenLoaiTaiKhoan == "Nhân viên")
                k = 1;
            cbxLoaiTK.SelectedIndex = k;
            txtTenDN.Text = tk.TenDangNhap.Trim();
            //txtMK.Text = tk.MatKhau;
            txtMaNV2.Text = tk.MaNV.Trim();
        }

        private void dgvKH_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var kh = dgvKH.SelectedItem as KhachHang;
            txtSDTKH.Text = kh.SDT.Trim();
        }

        //private void Cam_Click(object sender, RoutedEventArgs e)
        //{
        //    SolidColorBrush myBrush = new SolidColorBrush(Colors.Orange);
        //    Home.Background = myBrush;
        //    RB.Background = myBrush;
        //    Tab1.Background = myBrush;
        //}

        //private void XanhLam_Click(object sender, RoutedEventArgs e)
        //{
        //    SolidColorBrush myBrush = new SolidColorBrush(Colors.Azure);
        //    Home.Background = myBrush;
        //    RB.Background = myBrush;
        //    Tab1.Background = myBrush;
        //}

        //private void XanhLa_Click(object sender, RoutedEventArgs e)
        //{
        //    SolidColorBrush myBrush = new SolidColorBrush(Colors.Green);
        //    Home.Background = myBrush;
        //    RB.Background = myBrush;
        //    Tab1.Background = myBrush;
        //}

        //private void LamMoi_Click(object sender, RoutedEventArgs e)
        //{
        //    SolidColorBrush myBrush = new SolidColorBrush(Colors.White);
        //    Home.Background = myBrush;
        //    RB.Background = myBrush;
        //    Tab1.Background = myBrush;
        //    txtDiaChiNV.Clear();
        //    txtMaNV2.Clear();
        //    txtMK.Clear();
        //    txtSDTKH.Clear();
        //    txtSDTNV.Clear();
        //    txtTenDN.Clear();
        //    txtTenNV.Clear();
        //    cbxChucVu.SelectedIndex = -1;
        //    cbxLoaiTK.SelectedIndex = -1;
        //}

        private void RibbonApplicationMenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void RibbonApplicationMenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
