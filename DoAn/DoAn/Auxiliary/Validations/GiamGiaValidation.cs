﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace DoAn.Auxiliary.Validations
{
    public class GiamGiaValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {

            Regex regex = new Regex(@"^[0-9]+$");
            if (!regex.IsMatch(value.ToString()))
            {
                return new ValidationResult(false, "Giảm giá là kiểu số");
            }
            int giamgia = 0;
            bool isGiamGia = Int32.TryParse(value.ToString(), out giamgia);
            if(!isGiamGia || (giamgia < -1 || giamgia > 100))
            {
                return new ValidationResult(false, "Giá trị không phù hợp");
            }
            return ValidationResult.ValidResult;
        }
    }
}
