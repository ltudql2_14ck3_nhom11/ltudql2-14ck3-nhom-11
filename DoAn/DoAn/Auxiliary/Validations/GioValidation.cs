﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace DoAn.Auxiliary.Validations
{
    class GioValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            int gio = 0;
            bool isGio = Int32.TryParse(value.ToString(), out gio);
            if (!isGio)
            {
                return new ValidationResult(false, "[0->24] giờ");
            }
            if(gio < 0 || gio > 24)
            {
                return new ValidationResult(false, "[0->24] giờ");
            }
            return ValidationResult.ValidResult;
        }
    }
}
