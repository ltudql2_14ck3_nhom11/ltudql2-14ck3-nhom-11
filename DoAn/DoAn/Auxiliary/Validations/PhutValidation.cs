﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace DoAn.Auxiliary.Validations
{
    class PhutValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            int phut = 0;
            bool isPhut = Int32.TryParse(value.ToString(), out phut);
            if (!isPhut)
            {
                return new ValidationResult(false, "[0->60] phút");
            }
            if(phut < 0 || phut > 60)
            {
                return new ValidationResult(false, "[0->60] phút");
            }
            return ValidationResult.ValidResult;
        }
    }
}
