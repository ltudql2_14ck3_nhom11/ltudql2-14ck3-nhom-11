﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Model;

namespace DoAn
{
    /// <summary>
    /// Interaction logic for DoiMatKhau.xaml
    /// </summary>
    public partial class DoiMatKhau : Window
    {
        QLRapChieuPhimDBEntities db = new QLRapChieuPhimDBEntities();
        string mtk = null;

        public DoiMatKhau(string MaTK)
        {
            InitializeComponent();
            mtk = MaTK;
        }

        public static byte[] encryptData(string data)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider md5Hasher = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] hashedBytes;
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            hashedBytes = md5Hasher.ComputeHash(encoder.GetBytes(data));
            return hashedBytes;
        }
        public static string md5(string data)
        {
            return BitConverter.ToString(encryptData(data)).Replace("-", "").ToLower();
        }

        private void btnDMK_Click(object sender, RoutedEventArgs e)
        {
            if ( txtPC.Password.ToString() == "" ||txtP.Password.ToString() == "" || txtRP.Password.ToString() == "" || txtP.Password.ToString() != txtRP.Password.ToString())
            {
                MessageBox.Show("Dữ liệu nhập vào chưa chính xác!");
                return;
            }
            string mkc = null;
            mkc = md5(txtPC.Password.ToString());
            int tk = db.TaiKhoans.Where(m => m.MatKhau == mkc  && m.MaTaiKhoan == mtk && m.BiXoa == false).Count();
            if (tk <= 0)
            {
                MessageBox.Show("Mật khẩu cũ của bạn không chính xác!");
            }
            else
            {
                var result = db.TaiKhoans.Where(k => k.MaTaiKhoan == mtk).Select(k => k).ToList();
                foreach (var r in result)
                {
                    try
                    {
                        string mk = md5(txtP.Password.ToString());
                        r.MatKhau = mk;
                        db.SaveChanges();
                        MessageBox.Show("Mật khẩu của bạn đã được cập nhật!");
                    }
                    catch
                    {
                        MessageBox.Show("Cập nhật không thành công!");
                        return;
                    }
                    break;
                }
            }

        }

        private void btnThoat_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
