﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DoAn
{
    /// <summary>
    /// Interaction logic for BaoCao.xaml
    /// </summary>
    public partial class BaoCao : UserControl
    {
        public BaoCao()
        {
            InitializeComponent();
            cbxNam.SelectedIndex = 0;
        }

        private void report_Load(int nam)
        {
            var source = new Microsoft.Reporting.WinForms.ReportDataSource();
            var dataSet = new DataSet();
            dataSet.BeginInit();
            source.Name = "DataSet";
            source.Value = dataSet.BaoBieu;
            report.LocalReport.DataSources.Add(source);
            report.LocalReport.ReportEmbeddedResource = "DoAn.Report.rdlc";
            dataSet.EndInit();

            var adapter = new DataSetTableAdapters.BaoBieuTableAdapter();
            adapter.ClearBeforeFill = true;
            adapter.Fill(dataSet.BaoBieu,nam.ToString());

            report.RefreshReport();
        }

        private void cbxNam_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.cbxNam.SelectedItem == null)
                return;
            int nam =(int) this.cbxNam.SelectedItem;
            report_Load(nam);
        }
    }
}
