﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoAn.UControl
{
    /// <summary>
    /// Interaction logic for PosterFilm.xaml
    /// </summary>
    public partial class PosterFilm : UserControl
    {
        public static DependencyProperty PosterProperty;
        public static DependencyProperty NamePosterProperty;
        public PosterFilm()
        {
            InitializeComponent();
        }
        static PosterFilm()
        {
            PosterProperty = DependencyProperty.Register("Poster", typeof(string), typeof(PosterFilm));
            NamePosterProperty = DependencyProperty.Register("NamePoster", typeof(string), typeof(PosterFilm));
        }
        public string Poster
        {
            get { return (string)GetValue(PosterProperty);}
            set { SetValue(PosterProperty, value); }
        }
        public string NamePoster
        {
            get { return (string)GetValue(NamePosterProperty); }
            set { SetValue(NamePosterProperty, value); }
        }
    }
}
