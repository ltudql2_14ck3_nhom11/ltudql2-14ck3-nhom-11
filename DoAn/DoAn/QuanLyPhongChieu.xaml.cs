﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DoAn
{
    /// <summary>
    /// Interaction logic for QuanLyPhongChieu.xaml
    /// </summary>
    public partial class QuanLyPhongChieu : UserControl
    {
        public QuanLyPhongChieu()
        {
            ViewModel.DataViewModel.ManHinhDangChay = 12;
            ViewModel.DataViewModel.PageSize = 10;
            InitializeComponent();
        }
    }
}
