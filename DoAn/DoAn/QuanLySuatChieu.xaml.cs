﻿using System.Windows;
using ViewModel;

namespace DoAn
{
    /// <summary>
    /// Interaction logic for QuanLySuatChieu.xaml
    /// </summary>
    public partial class QuanLySuatChieu : Window
    {
        string maTK = null;
        public QuanLySuatChieu(string mtk)
        {
            ViewModel.DataViewModel.ManHinhDangChay = 2;
            DataViewModel.PageSize = 5;
            InitializeComponent();
            maTK = mtk;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult mbr = MessageBox.Show("Xác nhận thoát", "Thoát", MessageBoxButton.YesNo);
            if (mbr == MessageBoxResult.No)
            {
                return;
            }
            this.Close();
        }

        private void btnDMK_Click(object sender, RoutedEventArgs e)
        {
            DoiMatKhau frm = new DoiMatKhau(maTK);
            this.Hide();
            frm.ShowDialog();
            this.ShowDialog();
        }
    }
}
