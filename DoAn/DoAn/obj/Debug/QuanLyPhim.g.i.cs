﻿#pragma checksum "..\..\QuanLyPhim.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "8BE491CE395C28D3ED9B21DA38372209"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using DoAn;
using DoAn.Auxiliary.Converters;
using DoAn.Auxiliary.Validations;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using ViewModel;


namespace DoAn {
    
    
    /// <summary>
    /// QuanLyPhim
    /// </summary>
    public partial class QuanLyPhim : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\QuanLyPhim.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DoAn.QuanLyPhim MyWindow;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\QuanLyPhim.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtTimKiemPhim;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\QuanLyPhim.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgvDSP;
        
        #line default
        #line hidden
        
        
        #line 126 "..\..\QuanLyPhim.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtMaPhim;
        
        #line default
        #line hidden
        
        
        #line 134 "..\..\QuanLyPhim.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtTenPhim;
        
        #line default
        #line hidden
        
        
        #line 141 "..\..\QuanLyPhim.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxTheLoai;
        
        #line default
        #line hidden
        
        
        #line 152 "..\..\QuanLyPhim.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxNhaCC;
        
        #line default
        #line hidden
        
        
        #line 164 "..\..\QuanLyPhim.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxNVQL;
        
        #line default
        #line hidden
        
        
        #line 176 "..\..\QuanLyPhim.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtND;
        
        #line default
        #line hidden
        
        
        #line 181 "..\..\QuanLyPhim.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnLoad;
        
        #line default
        #line hidden
        
        
        #line 184 "..\..\QuanLyPhim.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox imagePhoto;
        
        #line default
        #line hidden
        
        
        #line 187 "..\..\QuanLyPhim.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnOk;
        
        #line default
        #line hidden
        
        
        #line 208 "..\..\QuanLyPhim.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnUpdate;
        
        #line default
        #line hidden
        
        
        #line 247 "..\..\QuanLyPhim.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgvDSP_Them;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DoAn;component/quanlyphim.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\QuanLyPhim.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.MyWindow = ((DoAn.QuanLyPhim)(target));
            return;
            case 2:
            this.txtTimKiemPhim = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.dgvDSP = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 4:
            this.txtMaPhim = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.txtTenPhim = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.cbxTheLoai = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.cbxNhaCC = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 8:
            this.cbxNVQL = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 9:
            this.txtND = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.btnLoad = ((System.Windows.Controls.Button)(target));
            return;
            case 11:
            this.imagePhoto = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.btnOk = ((System.Windows.Controls.Button)(target));
            return;
            case 13:
            this.btnUpdate = ((System.Windows.Controls.Button)(target));
            return;
            case 14:
            this.dgvDSP_Them = ((System.Windows.Controls.DataGrid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

