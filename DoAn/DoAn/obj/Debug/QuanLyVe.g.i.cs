﻿#pragma checksum "..\..\QuanLyVe.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "92B3753364C3B1542C5B705F08173986"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using DoAn;
using DoAn.Auxiliary.Converters;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using ViewModel;


namespace DoAn {
    
    
    /// <summary>
    /// QuanLyVe
    /// </summary>
    public partial class QuanLyVe : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 65 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnLogout;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnDMK;
        
        #line default
        #line hidden
        
        
        #line 144 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dpNgayDK1;
        
        #line default
        #line hidden
        
        
        #line 148 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dpNgayDK2;
        
        #line default
        #line hidden
        
        
        #line 152 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbNgayDK1;
        
        #line default
        #line hidden
        
        
        #line 153 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbNgayDK2;
        
        #line default
        #line hidden
        
        
        #line 154 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbMaVe;
        
        #line default
        #line hidden
        
        
        #line 156 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSet;
        
        #line default
        #line hidden
        
        
        #line 192 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxPhim;
        
        #line default
        #line hidden
        
        
        #line 208 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxSuat;
        
        #line default
        #line hidden
        
        
        #line 231 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxLoaiVe;
        
        #line default
        #line hidden
        
        
        #line 239 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnTaoVeOpen;
        
        #line default
        #line hidden
        
        
        #line 310 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtTenKH;
        
        #line default
        #line hidden
        
        
        #line 317 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rbtnKH;
        
        #line default
        #line hidden
        
        
        #line 322 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rbtnTT;
        
        #line default
        #line hidden
        
        
        #line 324 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel placeKHThanThiet;
        
        #line default
        #line hidden
        
        
        #line 327 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSDT;
        
        #line default
        #line hidden
        
        
        #line 331 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnKH_Exists;
        
        #line default
        #line hidden
        
        
        #line 356 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox cboxSDTMoi;
        
        #line default
        #line hidden
        
        
        #line 363 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox lbDiemTichLuy;
        
        #line default
        #line hidden
        
        
        #line 370 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnOK;
        
        #line default
        #line hidden
        
        
        #line 394 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPrint;
        
        #line default
        #line hidden
        
        
        #line 418 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox lbxHoaDon;
        
        #line default
        #line hidden
        
        
        #line 433 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dpTimKiemVe;
        
        #line default
        #line hidden
        
        
        #line 465 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnForward;
        
        #line default
        #line hidden
        
        
        #line 466 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBack;
        
        #line default
        #line hidden
        
        
        #line 475 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNext;
        
        #line default
        #line hidden
        
        
        #line 476 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnLast;
        
        #line default
        #line hidden
        
        
        #line 480 "..\..\QuanLyVe.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgVe;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DoAn;component/quanlyve.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\QuanLyVe.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.btnLogout = ((System.Windows.Controls.Button)(target));
            
            #line 65 "..\..\QuanLyVe.xaml"
            this.btnLogout.Click += new System.Windows.RoutedEventHandler(this.btnLogout_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.btnDMK = ((System.Windows.Controls.Button)(target));
            
            #line 83 "..\..\QuanLyVe.xaml"
            this.btnDMK.Click += new System.Windows.RoutedEventHandler(this.btnDMK_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.dpNgayDK1 = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 4:
            this.dpNgayDK2 = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 5:
            this.lbNgayDK1 = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.lbNgayDK2 = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.lbMaVe = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.btnSet = ((System.Windows.Controls.Button)(target));
            return;
            case 9:
            this.cbxPhim = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 10:
            this.cbxSuat = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 11:
            this.cbxLoaiVe = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 12:
            this.btnTaoVeOpen = ((System.Windows.Controls.Button)(target));
            
            #line 243 "..\..\QuanLyVe.xaml"
            this.btnTaoVeOpen.Click += new System.Windows.RoutedEventHandler(this.btnTaoVeOpen_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.txtTenKH = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.rbtnKH = ((System.Windows.Controls.RadioButton)(target));
            
            #line 321 "..\..\QuanLyVe.xaml"
            this.rbtnKH.Click += new System.Windows.RoutedEventHandler(this.rbtnKH_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.rbtnTT = ((System.Windows.Controls.RadioButton)(target));
            
            #line 322 "..\..\QuanLyVe.xaml"
            this.rbtnTT.Click += new System.Windows.RoutedEventHandler(this.rbtnTT_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.placeKHThanThiet = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 17:
            this.txtSDT = ((System.Windows.Controls.TextBox)(target));
            return;
            case 18:
            this.btnKH_Exists = ((System.Windows.Controls.Button)(target));
            return;
            case 19:
            this.cboxSDTMoi = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 20:
            this.lbDiemTichLuy = ((System.Windows.Controls.TextBox)(target));
            return;
            case 21:
            this.btnOK = ((System.Windows.Controls.Button)(target));
            return;
            case 22:
            this.btnPrint = ((System.Windows.Controls.Button)(target));
            
            #line 400 "..\..\QuanLyVe.xaml"
            this.btnPrint.Click += new System.Windows.RoutedEventHandler(this.btnPrint_Click);
            
            #line default
            #line hidden
            return;
            case 23:
            this.lbxHoaDon = ((System.Windows.Controls.ListBox)(target));
            return;
            case 24:
            this.dpTimKiemVe = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 25:
            this.btnForward = ((System.Windows.Controls.Button)(target));
            return;
            case 26:
            this.btnBack = ((System.Windows.Controls.Button)(target));
            return;
            case 27:
            this.btnNext = ((System.Windows.Controls.Button)(target));
            return;
            case 28:
            this.btnLast = ((System.Windows.Controls.Button)(target));
            return;
            case 29:
            this.dgVe = ((System.Windows.Controls.DataGrid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

