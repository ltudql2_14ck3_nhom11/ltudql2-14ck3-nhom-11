﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DoAn
{
    /// <summary>
    /// Interaction logic for QuanLyNhaCC.xaml
    /// </summary>
    public partial class QuanLyNhaCC : UserControl
    {
        public QuanLyNhaCC()
        {
            ViewModel.DataViewModel.ManHinhDangChay = 11;
            ViewModel.DataViewModel.PageSize = 10;
            InitializeComponent();
        }
    }
}
