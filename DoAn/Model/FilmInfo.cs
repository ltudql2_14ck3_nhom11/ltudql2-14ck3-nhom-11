﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Model
{
    public class FilmInfo
    {
        private string _MaPhim;
        public string MaPhim
        {
            get { return _MaPhim; }
            set { _MaPhim = value; }
        }
        private string _TenPhim;
        public string TenPhim
        {
            get { return _TenPhim; }
            set { _TenPhim = value; }
        }
        private BitmapImage _PosterPath;
        public BitmapImage PosterPath
        {
            get { return _PosterPath; }
            set { _PosterPath = value; }
        }
    }
}
