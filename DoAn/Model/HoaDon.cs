﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class HoaDon : INotifyPropertyChanged
    {
        void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        string _thoiGianIN;
        public string ThoiGianIN
        {
            get { return _thoiGianIN; }
            set
            {
                if (value == _thoiGianIN)
                    return;
                _thoiGianIN = value;
                OnPropertyChanged("ThoiGianIN");
            }
        }
        string _tenPhimIN;
        public string TenPhimIN
        {
            get { return _tenPhimIN; }
            set
            {
                if (value == _tenPhimIN)
                    return;
                _tenPhimIN = value;
                OnPropertyChanged("TenPhimIN");
            }

        }
        string _dinhDangIN;
        public string DinhDangIN
        {
            get { return _dinhDangIN; }
            set
            {
                if (value == _dinhDangIN)
                    return;
                _dinhDangIN = value;
                OnPropertyChanged("DinhDangIN");
            }
        }
        string _giaVeIN;
        public string GiaVeIN
        {
            get { return _giaVeIN; }
            set
            {
                if (value == _giaVeIN)
                    return;
                _giaVeIN = value;
                OnPropertyChanged("GiaVeIN");
            }
        }
        string _phongChieuChoNgoiIN;
        public string PhongChieuChoNgoiIN
        {
            get { return _phongChieuChoNgoiIN; }
            set
            {
                if (value == _phongChieuChoNgoiIN)
                    return;
                _phongChieuChoNgoiIN = value;
                OnPropertyChanged("PhongChieuChoNgoiIN");
            }
        }
        string _khachHangIN;

        public event PropertyChangedEventHandler PropertyChanged;

        public string KhachHangIN
        {
            get { return _khachHangIN; }
            set
            {
                if (value == _khachHangIN)
                    return;
                _khachHangIN = value;
                OnPropertyChanged("KhachHangIN");
            }
        }
    }
}
