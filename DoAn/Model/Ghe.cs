﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Model
{
    public class Ghe
    {
        string _tenGhe;
        public string TenGhe
        {
            get { return _tenGhe; }
            set
            {
                if (value == _tenGhe)
                    return;
                _tenGhe = value;
            }
        }
        bool _daChon;
        public bool DaChon
        {
            get { return _daChon; }
            set
            {
                if (value == _daChon)
                    return;
                _daChon = value;
            }
        }
        BitmapImage _hinhGhe;
        public BitmapImage HinhGhe
        {
            get { return _hinhGhe; }
            set
            {
                _hinhGhe = value;
            }
        }

    }
}
