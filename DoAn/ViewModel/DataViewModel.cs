﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Model;
using System.Windows.Media.Imaging;
using System.Windows.Input;
using System.Windows.Controls;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows;
using System.IO;
using Microsoft.Win32;
using System.Data.Entity;

namespace ViewModel
{
    public class DataViewModel : INotifyPropertyChanged
    {
        private QLRapChieuPhimDBEntities db = new QLRapChieuPhimDBEntities();

        public DataViewModel()
        {
            DSNam = new ObservableCollection<int>();
            loadNam();
            INHoaDonL = new ObservableCollection<HoaDon>();

            listPhongChieu = new ObservableCollection<PhongChieu>();
            loadPhongChieu();

            listSuatChieu = new ObservableCollection<SuatChieu>();
            listSuatChieuQuanLyVe = new ObservableCollection<SuatChieu>();
            this.CurPage = 1;
            loadSuatChieu(this.CurPage);

            listPhim = new ObservableCollection<FilmInfo>();
            loadPhim();

            listPhimQuanLyVe = new ObservableCollection<FilmInfo>();

            listLichChieu = new ObservableCollection<LichChieu>();
            loadLichChieu();

            listVe = new ObservableCollection<Ve>();
            loadVe(this.CurPage);

            listLoaiVe = new ObservableCollection<LoaiVe>();
            loadLoaiVe(this.CurPage);

            listDinhDang = new ObservableCollection<DinhDang>();
            loadDinhDang();
            DanhSachGhe = new ObservableCollection<Ghe>();

            TaoNgayCommandMethod();
            TaoSuatChieuCommandMethod();
            TimKiemSuatChieuCommandMethod();
            XoaSuatChieuCommandMethod();
            SuaSuatChieuCommandMethod();
            XoaLichChieuCommandMethod();
            TimKiemLichChieuCommandMethod();
            LoadLaiSuatChieuCommandMethod();
            //QuanLyVe
            XacDinhDanhSachPhimCommandMethod();
            PhimThayDoiSuatChieuCommandMethod();
            SuatChieuSelectChangesCommandMethod();
            KiemTraSDTKhachHangCommandMethod();
            BanVeCommandMethod();
            XoaVeCommandMethod();
            LoaiVeSelectChangesCommandMethod();
            TimKiemVeCommandMethod();
            //TaoVe

            TimKiemLoaiVeCommandMethod();
            ThemMoiLoaiVeCommandMethod();
            XoaLoaiVeCommandMethod();
            CapNhatLoaiVeCommandMethod();
            ThaoTacCapNhatLoaiVeCommandMethod();
            //paged list
            ForwardButtonCommandMethod();
            BackButtonCommandMethod();
            NextButtonCommandMethod();
            LastButtonCommandMethod();
            // constant
            isKhachHangThanThiet = true;
            /////////////////////////////////////////////
            listNhaCC = new ObservableCollection<NhaCCPhim>();
            loadNhaCC();

            listLoaiPhim = new ObservableCollection<LoaiPhim>();
            loadLoaiPhim();

            listPC = new ObservableCollection<PhongChieu>();
            loadPC();

            listNV = new ObservableCollection<NhanVien>();
            loadNV();

            CloseWindowCommandMethod();
            //CurWindowState = WindowState.Maximized;
            //CmdMaxMethod();
            TimKiemNCCCommandMethod();
            ThemNhaCCCommandMethod();
            SuaNhaCCLoadCommandMethod();
            SuaNhaCCCMMethod();
            XoaNhaCCCMMethod();
            //LoaiPhim
            ThemLoaiPhimCommandMethod();
            TimKiemLoaiPhimCommandMethod();
            SuaLoaiPhimLoadCommandMethod();
            XoaLoaiPhimCommandMethod();
            SuaLoaiPhimCommmandMethod();
            //PhongChieu
            ThemPCCommandMethod();
            TimKiemPCCommandMethod();
            SuaPCLoadCommandMethod();
            XoaPCCommandMethod();
            SuaPCCommmandMethod();
            //Phim
            listDSPhim = new ObservableCollection<Phim>();
            loadDSPhim();

            ChonHinhCommandMethod();
            TimKiemPhimCommandMethod();
            PhimLoadCommandMethod();
            ThemPhimCommandMethod();
            XoaPhimCommandMethod();
            SuaPhimCommandMethod();

            //THI/////////////////////////////////////////////////////
            //////////////////////////////////////////f//////////////////////////////////
            listNhanVien = new ObservableCollection<NhanVien>();
            loadNhanVien();

            listTaiKhoan = new ObservableCollection<TaiKhoan>();
            loadTaiKhoan();

            listChucVu = new ObservableCollection<LoaiNV>();
            loadChucVu();

            listLoaiTaiKhoan = new ObservableCollection<LoaiTaiKhoan>();
            loadLoaiTaiKhoan();

            //KH//////////////////////////////////////////////////
            listKhachHang = new ObservableCollection<KhachHang>();
            loadKhachHang();
            XoaKhachHangCommandMethod();
            //CapNhatKhachHangCommandMethod();
            TimKiemKhachHangCommandMethod();
            LamMoiKhachHangCommandMethod();
            /////////////////////////////////////////////////////

            TaoNhanVienCommandMethod();
            TaoTaiKhoanCommandMethod();
            XoaNhanVienCommandMethod();
            XoaTaiKhoanCommandMethod();
            CapNhatNhanVienCommandMethod();
            CapNhatTaiKhoanCommandMethod();
        }

        private ObservableCollection<PhongChieu> _listPhongChieu;
        public ObservableCollection<PhongChieu> listPhongChieu
        {
            get { return _listPhongChieu; }
            set { _listPhongChieu = value; }
        }
        private void loadPhongChieu()
        {
            if (ManHinhDangChay != 2)
                return;
            var phongChieu = db.PhongChieus.Where(p => p.BiXoa == false);
            foreach (var pc in phongChieu)
            {
                listPhongChieu.Add(pc);
            }
        }

        private ObservableCollection<SuatChieu> _listSuatChieu;
        public ObservableCollection<SuatChieu> listSuatChieu
        {
            get { return _listSuatChieu; }
            set
            {
                if (value == _listSuatChieu)
                {
                    return;
                }
                _listSuatChieu = value;
                OnPropertyChanged("listSuatChieu");
            }
        }
        private ObservableCollection<SuatChieu> _listSuatChieuQuanLyVe;
        public ObservableCollection<SuatChieu> listSuatChieuQuanLyVe
        {
            get { return _listSuatChieuQuanLyVe; }
            set
            {
                if (value == _listSuatChieuQuanLyVe)
                {
                    return;
                }
                _listSuatChieuQuanLyVe = value;
                OnPropertyChanged("listSuatChieuQuanLyVe");
            }
        }
        private void loadSuatChieu(int curPage)
        {
            if (ManHinhDangChay != 2)
                return;

            listSuatChieu.Clear();
            var suatchieu = db.SuatChieus.Where(s => s.BiXoa == false);
            TotalPage = (int)Math.Ceiling(suatchieu.Count() * 1.0 / PageSize);
            var suatchieus = suatchieu.OrderByDescending(s => s.Ngay).Skip((curPage - 1) * PageSize).Take(PageSize).ToList();

            foreach (var c in suatchieus)
            {
                SuatChieu sc = new SuatChieu
                {
                    Ngay = DateTime.Parse(c.Ngay.ToShortDateString()),
                    Suat = c.Suat,
                    ThoiGian = c.ThoiGian,
                    Phim = c.Phim,
                    PhongChieu = c.PhongChieu
                };
                listSuatChieu.Add(sc);
            }
        }
        private ObservableCollection<FilmInfo> _listPhim;
        public ObservableCollection<FilmInfo> listPhim
        {
            get { return _listPhim; }
            set
            {
                if (_listPhim == value)
                    return;
                _listPhim = value;
                OnPropertyChanged("listPhim");
            }
        }
        private ObservableCollection<FilmInfo> _listPhimQuanLyVe;
        public ObservableCollection<FilmInfo> listPhimQuanLyVe
        {
            get { return _listPhimQuanLyVe; }
            set
            {
                if (value == _listPhimQuanLyVe)
                    return;
                _listPhimQuanLyVe = value;
                OnPropertyChanged("ListPhimQuanLyVe");
            }
        }
        private void loadPhim()
        {
            if (ManHinhDangChay != 2)
                return;
            foreach (var p in db.Phims.Where(p => p.BiXoa == false))
            {
                listPhim.Add(new FilmInfo() { MaPhim = p.MaPhim, TenPhim = p.TenPhim, PosterPath = imagePath(p.PosterFilm) });
            }
        }
        private BitmapImage imagePath(string fileName)
        {
            string strUri = string.Format(@"pack://application:,,,/DoAn;component/Auxiliary/Resouce/Image/{0}", fileName);
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.UriSource = new Uri(strUri);
            image.EndInit();
            return image;
        }

        private ObservableCollection<LichChieu> _listLichChieu;
        public ObservableCollection<LichChieu> listLichChieu
        {
            get { return _listLichChieu; }
            set { _listLichChieu = value; }
        }
        private void loadLichChieu()
        {
            if (ManHinhDangChay != 2)
                return;
            var data = db.LichChieus.Where(l => l.BiXoa == false).ToList();
            foreach (var lc in data)
            {
                listLichChieu.Add(lc);
            }
        }

        private ObservableCollection<Ve> _listVe;
        public ObservableCollection<Ve> listVe
        {
            get { return _listVe; }
            set { _listVe = value; }
        }
        private void loadVe(int curPage)
        {
            if (ManHinhDangChay != 1)
                return;
            if (ChonPhim == null)
            {
                return;
            }
            FilmInfo phim = ChonPhim as FilmInfo;
            listVe.Clear();

            var result = db.Ves.Where(v => v.BiXoa == false && v.SuatChieu.MaPhim.Equals(phim.MaPhim));
            TotalPage = (int)Math.Ceiling(result.Count() * 1.0 / PageSize);
            result = result.OrderByDescending(v => v.MaVe).Skip((curPage - 1) * PageSize).Take(PageSize);
            var listV = result.Select(v => new { v.MaVe, v.TenKH, v.SuatChieu, v.SDT, v.LoaiVe1, v.SuatChieu.Phim, v.SuatChieu.PhongChieu, v.Ghe });
            foreach (var r in listV)
            {
                Phim p = r.SuatChieu.Phim;

                SuatChieu sc = r.SuatChieu;
                sc.Phim = p;
                sc.PhongChieu = r.SuatChieu.PhongChieu;

                LoaiVe lv = r.LoaiVe1;

                Ve v = new Ve
                {
                    MaVe = r.MaVe,
                    LoaiVe1 = lv,
                    SuatChieu = sc,
                    TenKH = r.TenKH,
                    Ghe = r.Ghe,
                    SDT = string.IsNullOrEmpty(r.SDT) ? "null" : r.SDT
                };
                listVe.Add(v);
            }

        }

        private ObservableCollection<LoaiVe> _listLoaiVe;
        public ObservableCollection<LoaiVe> listLoaiVe
        {
            get { return _listLoaiVe; }
            set
            {
                if (_listLoaiVe == value)
                    return;
                _listLoaiVe = value;
                OnPropertyChanged("ListLoaiVe");
            }
        }
        private void loadLoaiVe(int curPage)
        {
            if (ManHinhDangChay == 3 || ManHinhDangChay == 1)
            {
                var result = db.LoaiVes.Where(l => l.BiXoa == false);
                TotalPage = (int)Math.Ceiling(result.Count() * 1.0 / PageSize);
                result = result.OrderByDescending(l => l.MaLoaiVe).Skip(((curPage) - 1) * PageSize).Take(PageSize);
                var listLVe = result.Select(l => new { l.MaLoaiVe, l.TenLoaiVe, l.LoaiKhachHang, l.GiaVe, l.DinhDang }).ToList();
                listLoaiVe.Clear();
                foreach (var r in result)
                {
                    LoaiVe lv = new LoaiVe
                    {
                        MaLoaiVe = r.MaLoaiVe,
                        TenLoaiVe = r.TenLoaiVe,
                        LoaiKhachHang = r.LoaiKhachHang,
                        GiaVe = r.GiaVe,
                        DinhDang = r.DinhDang
                    };
                    listLoaiVe.Add(lv);
                }
                db.SuatChieus.Where(k => k.BiXoa == false).Select(k => k.Ngay.Year).Distinct();
            }

        }

        private ObservableCollection<DinhDang> _listDinhDang;

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<DinhDang> listDinhDang
        {
            get { return _listDinhDang; }
            set { _listDinhDang = value; }
        }
        private void loadDinhDang()
        {
            if (ManHinhDangChay != 3)
                return;
            foreach (var d in db.DinhDangs.ToList())
            {
                listDinhDang.Add(d);
            }
        }

        //property
        void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        DateTime? _ngayLC;
        public DateTime? NgayLC
        {
            get { return _ngayLC; }
            set
            {
                if (value == _ngayLC)
                    return;
                _ngayLC = value;
                OnPropertyChanged("NgayLC");
            }
        }

        string _giamGia;
        public string GiamGia
        {
            get { return _giamGia; }
            set
            {
                if (value == _giamGia)
                {
                    return;
                }
                _giamGia = value;
                OnPropertyChanged("GiamGia");
            }
        }
        string _gio;
        public string Gio
        {
            get { return _gio; }
            set
            {
                if (value == _gio)
                {
                    return;
                }
                _gio = value;
                OnPropertyChanged("Gio");
            }
        }
        string _phut;
        public string Phut
        {
            get { return _phut; }
            set
            {
                if (value == _phut)
                    return;
                _phut = value;
                OnPropertyChanged("Phut");
            }
        }
        object _ngayChieu;
        public object NgayChieu
        {
            get { return _ngayChieu; }
            set
            {
                if (value == _ngayChieu)
                    return;
                _ngayChieu = value;
                OnPropertyChanged("NgayChieu");
            }
        }
        object _phongChieu;
        public object PhongChieu
        {
            get { return _phongChieu; }
            set
            {
                if (_phongChieu == value)
                    return;
                _phongChieu = value;
                OnPropertyChanged("PhongChieu");
            }
        }
        object _chonPhim;
        public object ChonPhim
        {
            get { return _chonPhim; }
            set
            {
                if (value == _chonPhim)
                    return;
                _chonPhim = value;
                OnPropertyChanged("ChonPhim");
            }
        }
        DateTime? _timKiemTuNgay;
        public DateTime? TimKiemTuNgay
        {
            get { return _timKiemTuNgay; }
            set
            {
                if (value == _timKiemTuNgay)
                    return;
                _timKiemTuNgay = value;
                OnPropertyChanged("TimKiemTuNgay");
            }
        }
        DateTime? _timKiemDenNgay;
        public DateTime? TimKiemDenNgay
        {
            get { return _timKiemDenNgay; }
            set
            {
                if (value == _timKiemDenNgay)
                    return;
                _timKiemDenNgay = value;
                OnPropertyChanged("TimKiemDenNgay");
            }
        }

        object _timKiemPhim;
        public object TimKiemPhim
        {
            get { return _timKiemPhim; }
            set
            {
                if (value == _timKiemPhim)
                    return;
                _timKiemPhim = value;
                OnPropertyChanged("TimKiemPhim");
            }
        }
        object _timKiemPhongChieu;
        public object TimKiemPhongChieu
        {
            get { return _timKiemPhongChieu; }
            set
            {
                if (value == _timKiemPhongChieu)
                    return;
                _timKiemPhongChieu = value;
                OnPropertyChanged("TimKiemPhongChieu");
            }
        }
        Ghe _chonGhe;
        public Ghe ChonGhe
        {
            get { return _chonGhe; }
            set
            {
                if (_chonGhe == value)
                    return;
                _chonGhe = value;
                OnPropertyChanged("ChonGhe");
            }
        }

        ObservableCollection<Ghe> _danhSachGhe;
        public ObservableCollection<Ghe> DanhSachGhe
        {
            get { return _danhSachGhe; }
            set
            {
                if (value == _danhSachGhe)
                    return;
                _danhSachGhe = value;
                OnPropertyChanged("DanhSachGhe");
            }
        }
        string _tenKhachHang;
        public string TenKhachHang
        {
            get { return _tenKhachHang; }
            set
            {
                if (value == _tenKhachHang)
                    return;
                _tenKhachHang = value;
                OnPropertyChanged("TenKhachHang");
            }
        }
        string _sdtKhachHang;
        public string SDTKhachHang
        {
            get { return _sdtKhachHang; }
            set
            {
                if (value == _sdtKhachHang)
                    return;
                _sdtKhachHang = value;
                OnPropertyChanged("SDTKhachHang");
            }

        }
        string _diemTichLuyKhachHang;
        public string DiemTichLuyKhachHang
        {
            get { return _diemTichLuyKhachHang; }
            set
            {
                if (value == _diemTichLuyKhachHang)
                    return;
                _diemTichLuyKhachHang = value;
                OnPropertyChanged("DiemTichLuyKhachHang");
            }
        }
        object _suatChieu;
        public object SuatChieu
        {
            get { return _suatChieu; }
            set
            {
                if (value == _suatChieu)
                    return;
                _suatChieu = value;
                OnPropertyChanged("SuatChieu");
            }
        }
        object _loaiVe;
        public object LoaiVe
        {
            get { return _loaiVe; }
            set
            {
                if (value == _loaiVe)
                    return;
                _loaiVe = value;
                OnPropertyChanged("LoaiVe");

            }
        }
        bool? _isKhachHangThanThiet;
        public bool? isKhachHangThanThiet
        {
            get { return _isKhachHangThanThiet; }
            set
            {
                if (value == _isKhachHangThanThiet)
                    return;
                _isKhachHangThanThiet = value;
                OnPropertyChanged("isKhachHangThanThiet");
            }
        }

        bool? _isTaoMoiKhachHang;
        public bool? isTaoMoiKhachHang
        {
            get { return _isTaoMoiKhachHang; }
            set
            {
                if (value == _isTaoMoiKhachHang)
                    return;
                _isTaoMoiKhachHang = value;
                OnPropertyChanged("isTaoMoiKhachHang");
            }

        }
        string _tenLoaiVe;
        public string TenLoaiVe
        {
            get { return _tenLoaiVe; }
            set
            {
                if (value == _tenLoaiVe)
                    return;
                _tenLoaiVe = value;
                OnPropertyChanged("TenLoaiVe");
            }
        }

        object _dinhDangPhim;
        public object DinhDangPhim
        {
            get { return _dinhDangPhim; }
            set
            {
                if (value == _dinhDangPhim)
                    return;
                _dinhDangPhim = value;
                OnPropertyChanged("DinhDangPhim");
            }
        }

        string _loaiKhachHang;
        public string LoaiKhachHang
        {
            get { return _loaiKhachHang; }
            set
            {
                if (value == _loaiKhachHang)
                    return;
                _loaiKhachHang = value;
                OnPropertyChanged("LoaiKhachHang");
            }
        }

        string _giaVeLoaiVe;
        public string GiaVeLoaiVe
        {
            get { return _giaVeLoaiVe; }
            set
            {
                if (value == _giaVeLoaiVe)
                    return;
                _giaVeLoaiVe = value;
                OnPropertyChanged("GiaVeLoaiVe");
            }
        }
        string _maVeLoaiVe;
        public string MaVeLoaiVe
        {
            get { return _maVeLoaiVe; }
            set
            {
                if (_maVeLoaiVe == value)
                    return;
                _maVeLoaiVe = value;
                OnPropertyChanged("MaVeLoaiVe");
            }
        }


        public static int ManHinhDangChay;
        public static int PageSize = 5;
        int _curPage;
        public int CurPage
        {
            get { return _curPage; }
            set
            {
                if (value == _curPage) return;
                _curPage = value;
                OnPropertyChanged("CurPage");
            }
        }
        int _totalPage;
        public int TotalPage
        {
            get { return _totalPage; }
            set
            {
                if (value == _totalPage) return;
                _totalPage = value;
                OnPropertyChanged("TotalPage");
            }
        }
        // end property
        // begin command
        public ICommand TaoNgayCommand { get; set; }
        private void TaoNgayCommandMethod()
        {

            TaoNgayCommand = new RelayCommand<object>(
                (s) =>
                {
                    if (NgayLC == null)
                        return false;
                    if (string.IsNullOrEmpty(GiamGia))
                        return false;
                    Regex regex = new Regex(@"^[0-9]+$");
                    if (!regex.IsMatch(GiamGia.Trim()))
                    {
                        return false;
                    }
                    int giamgia = 0;
                    bool isGiamGia = Int32.TryParse(GiamGia.Trim(), out giamgia);
                    if (!isGiamGia)
                    {
                        return false;
                    }
                    if (giamgia < 0 || giamgia > 100)
                    {
                        return false;
                    }
                    return true;
                },
                (s) =>
                {
                    if (ManHinhDangChay != 2)
                        return;
                    if (string.IsNullOrEmpty(GiamGia))
                    {
                        return;
                    }
                    Regex regex = new Regex(@"^[0-9]+$");
                    if (!regex.IsMatch(GiamGia.Trim()))
                    {
                        return;
                    }
                    int giamgia = 0;
                    bool isGiamGia = Int32.TryParse(GiamGia.Trim(), out giamgia);
                    if (!isGiamGia)
                        return;
                    if (giamgia < 0 || giamgia > 100)
                        return;

                    var lctemp = db.LichChieus.FirstOrDefault(k => DateTime.Compare(k.Ngay, NgayLC.Value) == 0);
                    if (lctemp != null)
                    {
                        lctemp.GiamGia = giamgia;
                        int n = 0;
                        try
                        {
                            n = db.SaveChanges();
                        }
                        catch
                        {

                        }
                        if (n > 0)
                        {
                            var listLichChieuUI = listLichChieu.FirstOrDefault(k => DateTime.Compare(k.Ngay, NgayLC.Value) == 0);
                            listLichChieuUI.GiamGia = giamgia;
                        }

                    }
                    else
                    {
                        LichChieu lc = new LichChieu
                        {
                            Ngay = NgayLC.Value,
                            GiamGia = giamgia,
                            BiXoa = false
                        };
                        db.LichChieus.Add(lc);
                        int n = 0;
                        try
                        {
                            n = db.SaveChanges();
                        }
                        catch
                        {

                        }
                        if (n > 0)
                        {
                            listLichChieu.Add(lc);
                        }

                    }
                    listLichChieu.Clear();
                    loadLichChieu();
                    NgayLC = null;
                    GiamGia = string.Empty;
                }
                );
        }
        private void TaoNgayCommandMethod2()
        {
            TaoNgayCommand = new RelayCommand<UIElementCollection>(
                (s) => s != null,
                (s) =>
                      {
                          if (ManHinhDangChay != 2)
                              return;
                          DateTime? ngay = null;
                          int giamGia = 0;
                          bool isGiamGia = false;
                          foreach (var item in s)
                          {
                              WrapPanel wp = item as WrapPanel;
                              if (wp == null)
                                  continue;
                              foreach (var child in wp.Children)
                              {
                                  DatePicker dpNgay = child as DatePicker;
                                  if (dpNgay != null && dpNgay.Name.Equals("dpNgay1"))
                                  {
                                      if (dpNgay.SelectedDate == null)
                                      {
                                          return;
                                      }
                                      ngay = dpNgay.SelectedDate;
                                      continue;
                                  }
                                  TextBox txtGiamGia = child as TextBox;
                                  if (txtGiamGia != null && txtGiamGia.Name.Equals("txtGiamGia1"))
                                  {
                                      isGiamGia = Int32.TryParse(txtGiamGia.Text, out giamGia);
                                  }
                              }
                          }
                          if (!isGiamGia)
                          {
                              giamGia = 0;
                          }

                          LichChieu lc = new LichChieu
                          {
                              Ngay = DateTime.Parse(ngay.Value.ToShortDateString()),
                              GiamGia = giamGia
                          };
                          db.LichChieus.Add(lc);
                          int n = 0;
                          try
                          {
                              n = db.SaveChanges();
                          }
                          catch
                          {

                          }
                          if (n > 0)
                          {
                              listLichChieu.Add(lc);
                          }


                      }
               );
        }

        public ICommand TaoSuatChieuCommand { get; set; }
        private void TaoSuatChieuCommandMethod()
        {
            TaoSuatChieuCommand = new RelayCommand<object>(
                (s) =>
                {

                    if (NgayChieu == null)
                        return false;
                    if (PhongChieu == null)
                        return false;
                    if (Gio == null)
                        return false;
                    if (Phut == null)
                        return false;
                    return true;
                },
                (s) =>
                {
                    if (ManHinhDangChay != 2)
                        return;
                    string phongchieu = PhongChieu.ToString();
                    DateTime ngay = DateTime.Parse((NgayChieu as DateTime?).Value.ToShortDateString());
                    string thoigian = Gio + ":" + Phut;

                    var suatChieu = db.SuatChieus.FirstOrDefault(k => DateTime.Compare(k.Ngay, ngay) == 0 && k.ThoiGian.Equals(thoigian));
                    bool isFlag = false;
                    SuatChieu suatChieuTemp = null;
                    if (suatChieu != null)
                    {
                        suatChieu.ThoiGian = thoigian;
                        suatChieu.MaPChieu = phongchieu;
                        if (ChonPhim != null)
                        {
                            FilmInfo phim = ChonPhim as FilmInfo;
                            suatChieu.MaPhim = phim.MaPhim;
                        }
                    }
                    else
                    {
                        if (ChonPhim == null)
                            return;
                        FilmInfo phim = ChonPhim as FilmInfo;
                        int sosuat = db.SuatChieus.Where(k => k.Ngay == ngay).Select(k => k.Suat).Count();
                        SuatChieu sc = new SuatChieu
                        {
                            Ngay = ngay,
                            Suat = sosuat,
                            MaPhim = phim.MaPhim,
                            MaPChieu = phongchieu,
                            ThoiGian = thoigian,
                            BiXoa = false
                        };
                        suatChieuTemp = sc;
                        isFlag = true;
                        db.SuatChieus.Add(sc);
                    }
                    int n = 0;
                    try
                    {
                        n = db.SaveChanges();
                        if (n > 0 && !isFlag)
                        {
                            //var capNhatGiaoDien = listSuatChieu.FirstOrDefault(k => k.Ngay.Equals(ngay) && k.ThoiGian.Equals(thoigian));
                            //capNhatGiaoDien.ThoiGian = suatChieu.ThoiGian;
                            //capNhatGiaoDien.MaPhim = suatChieu.MaPhim;
                            //capNhatGiaoDien.MaPChieu = suatChieu.MaPChieu;
                            this.CurPage = 1;
                            loadSuatChieu(1);
                        }
                    }
                    catch
                    {

                    }
                    if (n > 0 && isFlag && suatChieuTemp != null)
                    {
                        listSuatChieu.Add(suatChieuTemp);
                        Gio = string.Empty;
                        Phut = string.Empty;
                        ChonPhim = null;
                        PhongChieu = null;
                        NgayChieu = null;
                    }
                }
                );
        }

        public ICommand XoaSuatChieuCommand { get; set; }
        private void XoaSuatChieuCommandMethod()
        {
            XoaSuatChieuCommand = new RelayCommand<object>(
                (s) => s != null,
                (s) =>
                {
                    if (ManHinhDangChay != 2)
                        return;
                    MessageBoxResult mbResult = MessageBox.Show("Chắc chắn xóa?", "Xóa suất chiếu", MessageBoxButton.YesNo);
                    if (mbResult == MessageBoxResult.No)
                    {
                        return;
                    }
                    SuatChieu sc = s as SuatChieu;
                    var result = db.SuatChieus.Single(p => p.Ngay == sc.Ngay && p.Suat == sc.Suat);
                    var soLuongVe = result.Ve.Count;
                    if (soLuongVe == 0)
                    {
                        db.SuatChieus.Remove(result);
                    }
                    else
                    {
                        result.BiXoa = true;
                    }
                    int n = 0;
                    try
                    {
                        n = db.SaveChanges();
                    }
                    catch
                    {
                        MessageBox.Show("Xóa thất bại");
                    }
                    if (n > 0)
                    {
                        listSuatChieu.Remove(sc);
                    }
                }
                );
        }

        public ICommand SuaSuatChieuCommand { get; set; }
        private void SuaSuatChieuCommandMethod()
        {
            SuaSuatChieuCommand = new RelayCommand<object>(
                (s) => s != null,
                (s) =>
                {
                    if (ManHinhDangChay != 2)
                        return;
                    SuatChieu sc = s as SuatChieu;

                    string[] thoigian = sc.ThoiGian.Split(':');
                    Gio = thoigian[0].Trim();
                    Phut = thoigian[1].Trim();
                    NgayChieu = sc.Ngay;
                    PhongChieu = sc.PhongChieu.MaPChieu.ToString();
                    Phim phim = sc.Phim;
                    FilmInfo filmInfo = new FilmInfo();
                    filmInfo.MaPhim = phim.MaPhim;
                    filmInfo.PosterPath = imagePath(phim.PosterFilm);
                    filmInfo.TenPhim = phim.TenPhim;
                    ChonPhim = filmInfo;
                }
                );
        }

        public ICommand LoadLaiSuatChieuCommand { get; set; }
        private void LoadLaiSuatChieuCommandMethod()
        {
            if (ManHinhDangChay != 2)
                return;
            LoadLaiSuatChieuCommand = new RelayCommand<object>(

                (s) => true,
                (s) =>
                {
                    this.CurPage = 1;
                    loadSuatChieu(CurPage);
                }
            );
        }
        public ICommand TimKiemSuatChieuCommand { get; set; }
        private void TimKiemSuatChieuCommandMethod()
        {
            TimKiemSuatChieuCommand = new RelayCommand<object>(
                (s) => true,
                (k) =>
                {
                    if (ManHinhDangChay != 2)
                        return;
                    if (TimKiemTuNgay == null)
                        return;
                    if (TimKiemDenNgay == null)
                        return;
                    if (TimKiemPhim == null)
                        return;
                    if (TimKiemPhongChieu == null)
                        return;

                    DateTime ngay1 = DateTime.Parse(TimKiemTuNgay.Value.ToShortDateString());
                    DateTime ngay2 = DateTime.Parse(TimKiemDenNgay.Value.ToShortDateString());
                    if (ngay1.CompareTo(ngay2) != -1)
                    {
                        ngay1 = DateTime.Parse(TimKiemDenNgay.Value.ToShortDateString());
                        ngay2 = DateTime.Parse(TimKiemTuNgay.Value.ToShortDateString());
                    }

                    string maphim = TimKiemPhim.ToString();

                    string maphong = this.TimKiemPhongChieu.ToString();

                    listSuatChieu.Clear();
                    var result = db.SuatChieus.Where(s => s.Ngay >= ngay1 && s.Ngay <= ngay2 && s.MaPhim == maphim && s.MaPChieu == maphong && s.BiXoa == false).Select(s => s).ToList();
                    foreach (var c in result)
                    {
                        Phim p = new Phim();
                        p.TenPhim = c.Phim.TenPhim;
                        p.MaPhim = c.MaPhim;
                        p.PosterFilm = c.Phim.PosterFilm;

                        PhongChieu pc = new PhongChieu()
                        {
                            TenPChieu = c.PhongChieu.TenPChieu,
                            MaPChieu = c.MaPChieu
                        };
                        SuatChieu sc = new SuatChieu()
                        {
                            Ngay = c.Ngay,
                            Suat = c.Suat,
                            ThoiGian = c.ThoiGian,
                            Phim = p,
                            PhongChieu = pc
                        };
                        listSuatChieu.Add(sc);
                    }

                }
                );
        }

        public ICommand XoaLichChieuCommand { get; set; }
        private void XoaLichChieuCommandMethod()
        {
            XoaLichChieuCommand = new RelayCommand<object>(
                (s) => s != null,
                (s) =>
                {
                    if (ManHinhDangChay != 2)
                        return;
                    MessageBoxResult mbResult = MessageBox.Show("Chắc chắn xóa?", "Xóa lịch chiếu", MessageBoxButton.YesNo);
                    if (mbResult == MessageBoxResult.No)
                    {
                        return;
                    }
                    LichChieu lichChieu = s as LichChieu;
                    var lc = db.LichChieus.FirstOrDefault(l => l.Ngay.CompareTo(lichChieu.Ngay) == 0);
                    var countSuatChieu = lc.SuatChieu.ToList();
                    if (lc != null && (countSuatChieu == null || countSuatChieu.Count == 0))
                    {
                        db.LichChieus.Remove(lc);
                    }
                    else
                    {
                        lc.BiXoa = true;
                    }
                    int n = 0;
                    try
                    {
                        n = db.SaveChanges();
                    }
                    catch
                    {

                    }
                    if (n > 0)
                    {
                        listLichChieu.Remove(lichChieu);
                    }
                }
                );
        }

        public ICommand TimKiemLichChieuCommand { get; set; }
        private void TimKiemLichChieuCommandMethod()
        {
            if (ManHinhDangChay != 2)
                return;
            TimKiemLichChieuCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    listLichChieu.Clear();
                    if (((DateTime?)s) == null)
                    {
                        loadLichChieu();
                    }
                    else
                    {
                        DateTime ngay = ((DateTime?)s).Value;
                        var result = db.LichChieus.FirstOrDefault(l => DateTime.Compare(l.Ngay, ngay) == 0 && l.BiXoa == false);
                        if (result != null)
                        {
                            listLichChieu.Clear();
                            listLichChieu.Add(result);
                        }
                    }
                }
                );
        }
        // begin QuanLyVe window
        public ICommand XacDinhDanhSachPhimCommand { get; set; }
        private void XacDinhDanhSachPhimCommandMethod()
        {
            if (ManHinhDangChay != 1)
                return;
            XacDinhDanhSachPhimCommand = new RelayCommand<object>(
                (s) => true,
                (k) =>
                {
                    if (TimKiemTuNgay == null || TimKiemDenNgay == null)
                    {
                        return;
                    }
                    DateTime ngaydk1 = DateTime.Parse(TimKiemTuNgay.Value.ToShortDateString());
                    DateTime ngaydk2 = DateTime.Parse(TimKiemDenNgay.Value.ToShortDateString());
                    if (ngaydk1.CompareTo(ngaydk2) != -1)
                    {
                        ngaydk1 = DateTime.Parse(TimKiemDenNgay.Value.ToShortDateString());
                        ngaydk2 = DateTime.Parse(TimKiemTuNgay.Value.ToShortDateString());
                    }
                    var result = db.SuatChieus.Where(s => s.Ngay >= ngaydk1 && s.Ngay <= ngaydk2 && s.BiXoa == false).Select(s => s.Phim).ToList().Distinct();
                    foreach (var r in result)
                    {
                        FilmInfo f = new FilmInfo()
                        {
                            TenPhim = r.TenPhim,
                            MaPhim = r.MaPhim,
                            PosterPath = imagePath(r.PosterFilm)
                        };
                        listPhimQuanLyVe.Add(f);
                    }

                }
                );
        }
        public ICommand PhimThayDoiSuatChieuCommand { get; set; }
        private void PhimThayDoiSuatChieuCommandMethod()
        {
            if (ManHinhDangChay != 1)
                return;
            PhimThayDoiSuatChieuCommand = new RelayCommand<object>(
                (s) => true,
                (k) =>
                {
                    if (k == null)
                    {
                        return;
                    }
                    if (TimKiemTuNgay == null || TimKiemDenNgay == null)
                    {
                        return;
                    }
                    this.CurPage = 1;
                    loadVe(this.CurPage);
                    DateTime ngaydk1 = DateTime.Parse(TimKiemTuNgay.Value.ToShortDateString());
                    DateTime ngaydk2 = DateTime.Parse(TimKiemDenNgay.Value.ToShortDateString());
                    if (ngaydk1.CompareTo(ngaydk2) != -1)
                    {
                        ngaydk1 = DateTime.Parse(TimKiemDenNgay.Value.ToShortDateString());
                        ngaydk2 = DateTime.Parse(TimKiemTuNgay.Value.ToShortDateString());
                    }
                    FilmInfo f = k as FilmInfo;
                    var listSC = db.SuatChieus.Where(s => s.Ngay >= ngaydk1 && s.Ngay <= ngaydk2 && s.MaPhim.Equals(f.MaPhim) && s.BiXoa == false)
                   .Select(s => new { s.Ngay, s.Suat, s.ThoiGian, s.Phim, s.PhongChieu, s.LichChieu })
                   .ToList();
                    if (listSC == null)
                    {
                        return;
                    }

                    listSuatChieuQuanLyVe.Clear();
                    foreach (var r in listSC)
                    {
                        Phim p = new Phim
                        {
                            MaPhim = r.Phim.MaPhim,
                            TenPhim = r.Phim.TenPhim,
                            DinhDang = r.Phim.DinhDang
                        };
                        PhongChieu pc = new PhongChieu
                        {
                            MaPChieu = r.PhongChieu.MaPChieu,
                            TenPChieu = r.PhongChieu.TenPChieu,
                            SoGhe = r.PhongChieu.SoGhe
                        };
                        LichChieu lc = r.LichChieu;
                        SuatChieu sc = new SuatChieu
                        {
                            Ngay = r.Ngay,
                            Suat = r.Suat,
                            ThoiGian = r.ThoiGian,
                            Phim = p,
                            PhongChieu = pc,
                            LichChieu = lc
                        };
                        listSuatChieuQuanLyVe.Add(sc);
                    }
                }
                );
        }
        public ICommand SuatChieuSelectChangesCommand { get; set; }
        private void SuatChieuSelectChangesCommandMethod()
        {
            if (ManHinhDangChay != 1)
                return;
            SuatChieuSelectChangesCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    if (s == null)
                        return;
                    loadGhe(s);
                }
                );
        }
        public ICommand LoaiVeSelectChangesCommand { get; set; }
        private void LoaiVeSelectChangesCommandMethod()
        {
            if (ManHinhDangChay != 1)
                return;
            LoaiVeSelectChangesCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    listLoaiVe.Clear();
                    var lv = db.LoaiVes.Where(l => l.BiXoa == false);
                    foreach (var l in lv)
                    {
                        listLoaiVe.Add(l);
                    }
                }
                );
        }
        private void loadGhe(object s)
        {
            if (ManHinhDangChay != 1)
                return;
            DanhSachGhe.Clear();
            SuatChieu sc = s as SuatChieu;
            int SoGhe = sc.PhongChieu.SoGhe.Value;
            for (int i = 0; i < SoGhe; i++)
            {
                Ghe ghe = new Ghe
                {
                    TenGhe = string.Format("{0}", i + 1),
                    HinhGhe = imagePath("desk-chair-on.png"),
                    DaChon = false
                };
                DanhSachGhe.Add(ghe);
            }
            var dsVe = db.Ves.Where(k => k.Ngay.Value.CompareTo(sc.Ngay) == 0 && k.Suat.Value == sc.Suat && k.BiXoa == false).ToList();
            foreach (var i in dsVe)
            {
                var k = i;
                if (!string.IsNullOrEmpty(i.Ghe))
                {
                    int tenGhe = Int32.Parse(i.Ghe.Trim());
                    Ghe ghe = DanhSachGhe.ElementAt(tenGhe - 1);

                    ghe.DaChon = true;
                    ghe.HinhGhe = imagePath("desk-chair-off.png");
                }
            }
        }
        public ICommand KiemTraSDTKhachHangCommand { get; set; }
        private void KiemTraSDTKhachHangCommandMethod()
        {
            if (ManHinhDangChay != 1)
                return;
            KiemTraSDTKhachHangCommand = new RelayCommand<object>(
                (s) => s != null,
                (s) =>
                {
                    if (s == null)
                    {
                        MessageBox.Show("Mời nhập số điện thoại");
                        return;
                    }
                    string sdt = s.ToString();
                    string soTIen = "";

                    try
                    {
                        KhachHang result = db.KhachHangs.Where(kh => kh.SDT == sdt && kh.BiXoa == false).Select(kh => kh).First();
                        soTIen = result.TongTien.ToString();
                    }
                    catch
                    {
                        soTIen = "Không tìm thấy";
                    }

                    DiemTichLuyKhachHang = soTIen;
                }
                );
        }
        ObservableCollection<HoaDon> _inHoaDon;
        public ObservableCollection<HoaDon> INHoaDonL
        {
            get { return _inHoaDon; }
            set
            {
                if (value == _inHoaDon)
                    return;
                _inHoaDon = value;
                OnPropertyChanged("INHoaDon");
            }
        }
        public ICommand BanVeCommand { get; set; }
        private void BanVeCommandMethod()
        {
            if (ManHinhDangChay != 1)
                return;
            BanVeCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    if(ChonPhim == null)
                    {
                        MessageBox.Show("Chưa chọn phim");
                        return;
                    }
                    if (SuatChieu == null)
                    {
                        MessageBox.Show("Chưa chọn suất chiếu");
                        return;
                    }

                    if (LoaiVe == null)
                    {
                        MessageBox.Show("Chọn loại vé");
                        return;
                    }

                    if (ChonGhe == null)
                    {
                        MessageBox.Show("Chọn chỗ ngồi");
                        return;
                    }

                    if (ChonGhe.DaChon)
                    {
                        MessageBox.Show("Ghế " + ChonGhe.TenGhe + " đã có người mua");
                        return;
                    }
                    MessageBoxResult mbr = MessageBox.Show("Xác nhận thêm vào", "Thêm mới", MessageBoxButton.YesNo);
                    if (mbr == MessageBoxResult.No)
                    {
                        return;
                    }
                    SuatChieu suatChieu = SuatChieu as SuatChieu;
                    decimal phanTramGiamGia = suatChieu.LichChieu.GiamGia == null ? 0 : suatChieu.LichChieu.GiamGia.Value;
                    LoaiVe loaiVe = LoaiVe as LoaiVe;
                    decimal giaVe = loaiVe.GiaVe == null ? 0 : loaiVe.GiaVe.Value;
                    if (giaVe > 1)
                    {
                        giaVe -= giaVe * (phanTramGiamGia / 100);
                    }

                    int maVe = 1;
                    while (true)
                    {
                        var kiemTra = db.Ves.Find("Ve" + maVe);
                        if (kiemTra == null)
                            break;
                        maVe++;
                    }
                    Ve ve = new Ve()
                    {
                        MaVe = "Ve" + maVe,
                        LoaiVe = loaiVe.MaLoaiVe,
                        Ngay = DateTime.Parse(suatChieu.Ngay.ToShortDateString()),
                        Suat = suatChieu.Suat,
                        TenKH = TenKhachHang == null ? "" : TenKhachHang,
                        Ghe = ChonGhe.TenGhe,
                        GiaVe = giaVe,
                        BiXoa = false
                    };

                    if (isTaoMoiKhachHang != null &&
                                isKhachHangThanThiet != null &&
                                !string.IsNullOrEmpty(DiemTichLuyKhachHang) &&
                                DiemTichLuyKhachHang.Equals("Không tìm thấy") &&
                                !isKhachHangThanThiet.Value &&
                                isTaoMoiKhachHang.Value &&
                                !string.IsNullOrEmpty(SDTKhachHang))
                    {
                        var kiemTraSDT = db.KhachHangs.Where(k => k.SDT.Equals(SDTKhachHang) && k.BiXoa == false).FirstOrDefault();
                        if (kiemTraSDT == null)
                        {
                            KhachHang khachHangMoi = new KhachHang()
                            {
                                SDT = SDTKhachHang,
                                TongTien = giaVe,
                                BiXoa = false
                            };
                            ve.KhachHang = khachHangMoi;
                        }
                    }
                    if (isTaoMoiKhachHang != null)
                    {
                        var temp = isTaoMoiKhachHang;
                    }
                    if ((isTaoMoiKhachHang == null || (isTaoMoiKhachHang != null && !isTaoMoiKhachHang.Value)) &&
                                isKhachHangThanThiet != null &&
                                !string.IsNullOrEmpty(DiemTichLuyKhachHang) &&
                                !DiemTichLuyKhachHang.Equals("Không tìm thấy") &&
                                !isKhachHangThanThiet.Value &&
                                !string.IsNullOrEmpty(SDTKhachHang))
                    {
                        var kiemTraSDT = db.KhachHangs.Where(k => k.SDT.Equals(SDTKhachHang) && k.BiXoa == false).FirstOrDefault();
                        if (kiemTraSDT != null)
                        {
                            ve.SDT = SDTKhachHang;
                            if (kiemTraSDT.TongTien.Value > 10000000)
                            {
                                ve.GiaVe = ve.GiaVe * Convert.ToDecimal(0.95);
                            }
                            kiemTraSDT.TongTien += Convert.ToDecimal(giaVe);
                        }
                    }
                    int n = 0;
                    try
                    {
                        db.Ves.Add(ve);
                        n = db.SaveChanges();
                    }
                    catch
                    {

                    }
                    if (n > 0)
                    {
                        //
                        INHoaDonL.Clear();
                        HoaDon INHoaDon = new HoaDon();
                        FilmInfo phim = ChonPhim as FilmInfo;
                        INHoaDon.ThoiGianIN = suatChieu.Ngay.ToString("dd/MM/yyyy") + "  " + suatChieu.ThoiGian;
                        INHoaDon.TenPhimIN = phim.TenPhim;
                        INHoaDon.DinhDangIN = loaiVe.DinhDang.TenDinhDang;
                        INHoaDon.GiaVeIN = giaVe.ToString() + " vnđ";
                        INHoaDon.PhongChieuChoNgoiIN = suatChieu.PhongChieu.TenPChieu + "--" + ve.Ghe;
                        INHoaDon.KhachHangIN = ve.TenKH;
                        INHoaDonL.Add(INHoaDon);
                        //
                        listVe.Add(ve);
                        SDTKhachHang = string.Empty;
                        TenKhachHang = string.Empty;
                        isKhachHangThanThiet = true;
                        DiemTichLuyKhachHang = string.Empty;
                        loadGhe((SuatChieu)this.SuatChieu);
                        this.CurPage = 1;
                        loadVe(this.CurPage);
                    }
                    if (isTaoMoiKhachHang != null && isTaoMoiKhachHang.Value)
                    {
                        isTaoMoiKhachHang = false;
                    }

                }
                );
        }

        public ICommand XoaVeCommand { get; set; }
        private void XoaVeCommandMethod()
        {
            if (ManHinhDangChay != 1)
                return;
            XoaVeCommand = new RelayCommand<Ve>(
                (s) => s != null,
                (s) =>
                {
                    if (s == null)
                        return;
                    MessageBoxResult mbResult = MessageBox.Show("Chắc chắn xóa?", "Xóa vé", MessageBoxButton.YesNo);
                    if (mbResult == MessageBoxResult.No)
                    {
                        return;
                    }

                    Ve ve = s as Ve;
                    var v = db.Ves.FirstOrDefault(k => k.MaVe.Equals(ve.MaVe));
                    db.Ves.Remove(v);
                    int n = 0;
                    try
                    {
                        n = db.SaveChanges();
                    }
                    catch
                    {

                    }
                    if (n > 0)
                    {
                        listVe.Remove(ve);
                        if (this.SuatChieu != null)
                            loadGhe((SuatChieu)this.SuatChieu);
                    }
                }
                );
        }

        public ICommand TimKiemVeCommand { get; set; }
        private void TimKiemVeCommandMethod()
        {
            if (ManHinhDangChay != 1)
                return;
            TimKiemVeCommand = new RelayCommand<DateTime?>(
                (s) =>
                {
                    if (ChonPhim == null)
                        return false;
                    return true;
                },
                (s) =>
                {
                    if (ChonPhim == null)
                    {
                        return;
                    }
                    listVe.Clear();
                    if (s == null)
                    {
                        var ve = db.Ves.Where(k => k.BiXoa == false);
                        foreach (var v in ve)
                        {
                            listVe.Add(v);
                        }
                    }
                    else
                    {
                        DateTime ngay = DateTime.Parse(s.Value.ToShortDateString());
                        FilmInfo p = ChonPhim as FilmInfo;
                        var ve = db.Ves.Where(k => k.BiXoa == false && k.Ngay.Value.CompareTo(ngay) == 0 && k.SuatChieu.MaPhim.Equals(p.MaPhim));
                        foreach (var v in ve)
                        {
                            listVe.Add(v);
                        }
                    }
                }
                );
        }
        // end QuanLyVe window
        // begin TaoVe window
        public ICommand TimKiemLoaiVeCommand { get; set; }
        private void TimKiemLoaiVeCommandMethod()
        {
            if (ManHinhDangChay != 3)
            {
                return;
            }
            TimKiemLoaiVeCommand = new RelayCommand<string>(
                (s) => true,
                (s) =>
                {
                    listLoaiVe.Clear();
                    if (string.IsNullOrEmpty(s))
                    {
                        this.CurPage = 1;
                        loadLoaiVe(this.CurPage);
                    }
                    else
                    {
                        var dsLoaiVe = db.LoaiVes.Where(lv => lv.BiXoa == false && lv.TenLoaiVe.Contains(s));
                        foreach (var i in dsLoaiVe)
                        {
                            listLoaiVe.Add(i);
                        }
                    }
                }
                );
        }
        public ICommand ThemMoiLoaiVeCommand { get; set; }
        private void ThemMoiLoaiVeCommandMethod()
        {
            ThemMoiLoaiVeCommand = new RelayCommand<object>(
                (s) =>
                {
                    if (string.IsNullOrEmpty(TenLoaiVe))
                        return false;
                    if (string.IsNullOrEmpty(LoaiKhachHang))
                        return false;
                    if (DinhDangPhim == null)
                        return false;
                    if (string.IsNullOrEmpty(GiaVeLoaiVe))
                        return false;
                    return true;
                },
                (s) =>
                {
                    if (ManHinhDangChay != 3)
                        return;
                    MaVeLoaiVe = string.Empty;
                    if (string.IsNullOrEmpty(TenLoaiVe))
                        return;
                    if (string.IsNullOrEmpty(LoaiKhachHang))
                        return;
                    if (DinhDangPhim == null)
                    {
                        return;
                    }
                    if (string.IsNullOrEmpty(GiaVeLoaiVe))
                        return;
                    MessageBoxResult mbr = MessageBox.Show("Xác nhận thêm", "Thêm loại vé", MessageBoxButton.YesNo);
                    if (mbr == MessageBoxResult.No)
                        return;
                    Regex regex = new Regex(@"^[0-9]+$");
                    if (!regex.IsMatch(GiaVeLoaiVe))
                    {
                        MessageBox.Show("Giá vé là kiểu số");
                        return;
                    }
                    decimal tienVe = Convert.ToDecimal(GiaVeLoaiVe.Trim());
                    if (tienVe < 0)
                    {
                        MessageBox.Show("Giá vé > 0");
                        return;
                    }
                    DinhDang dd = DinhDangPhim as DinhDang;
                    int count = db.LoaiVes.Count() + 1;
                    while (true)
                    {
                        var maloai = db.LoaiVes.Find("LV" + count);
                        if (maloai == null)
                            break;
                        count++;
                    }
                    Model.LoaiVe lv = new Model.LoaiVe()
                    {
                        MaLoaiVe = "LV" + count,
                        TenLoaiVe = this.TenLoaiVe,
                        LoaiKhachHang = this.LoaiKhachHang,
                        MaDinhDang = dd.MaDinhDang,
                        GiaVe = tienVe,
                        BiXoa = false
                    };
                    db.LoaiVes.Add(lv);
                    int n = 0;
                    try
                    {
                        n = db.SaveChanges();
                    }
                    catch
                    {

                    }
                    if (n > 0)
                    {
                        TenLoaiVe = string.Empty;
                        TenKhachHang = string.Empty;
                        LoaiKhachHang = string.Empty;
                        GiaVeLoaiVe = string.Empty;
                        listLoaiVe.Add(lv);
                    }

                }
                );
        }
        public ICommand XoaLoaiVeCommand { get; set; }
        private void XoaLoaiVeCommandMethod()
        {
            if (ManHinhDangChay != 3)
                return;
            XoaLoaiVeCommand = new RelayCommand<LoaiVe>(
                (s) => s != null,
                (s) =>
                {
                    if (s == null)
                        return;
                    var lv = db.LoaiVes.FirstOrDefault(k => k.MaLoaiVe.Equals(s.MaLoaiVe));
                    if (lv == null)
                        return;
                    int count = lv.Ve.Count;
                    if (count == 0)
                    {
                        db.LoaiVes.Remove(lv);
                    }
                    else
                    {
                        lv.BiXoa = true;
                    }
                    int n = 0;
                    try
                    {
                        n = db.SaveChanges();
                    }
                    catch
                    {

                    }
                    if (n > 0)
                    {
                        listLoaiVe.Remove(s);
                    }
                }
                );
        }
        public ICommand ThaoTacCapNhatLoaiVeCommand { get; set; }
        private void ThaoTacCapNhatLoaiVeCommandMethod()
        {
            if (ManHinhDangChay != 3)
                return;
            ThaoTacCapNhatLoaiVeCommand = new RelayCommand<LoaiVe>(
                (s) => s != null,
                (s) =>
                {
                    if (s == null)
                    {
                        return;
                    }
                    TenLoaiVe = s.TenLoaiVe;
                    TenKhachHang = s.LoaiKhachHang;
                    GiaVeLoaiVe = s.GiaVe.ToString();
                    DinhDangPhim = s.DinhDang;
                    MaVeLoaiVe = s.MaLoaiVe;
                    LoaiKhachHang = s.LoaiKhachHang;

                }
                );
        }
        public ICommand CapNhatLoaiVeCommand { get; set; }
        private void CapNhatLoaiVeCommandMethod()
        {
            if (ManHinhDangChay != 3)
            {
                return;
            }
            CapNhatLoaiVeCommand = new RelayCommand<string>(
                (s) =>
                {
                    if (string.IsNullOrEmpty(s))
                        return false;
                    return true;
                },
                (s) =>
                {
                    if (string.IsNullOrEmpty(s))
                        return;
                    string maLV = MaVeLoaiVe;
                    MaVeLoaiVe = string.Empty;
                    var lv = db.LoaiVes.FirstOrDefault(k => k.MaLoaiVe.Equals(maLV));
                    if (lv == null)
                        return;
                    Regex regex = new Regex(@"^[0-9]+$");
                    if (!regex.IsMatch(GiaVeLoaiVe))
                    {
                        MessageBox.Show("Giá vé là kiểu số");
                        return;
                    }
                    decimal tienVe = Convert.ToDecimal(GiaVeLoaiVe.Trim());
                    if (tienVe < 0)
                    {
                        MessageBox.Show("Giá vé > 0");
                        return;
                    }
                    DinhDang dd = DinhDangPhim as DinhDang;
                    lv.TenLoaiVe = TenLoaiVe;
                    lv.DinhDang = dd;
                    lv.GiaVe = tienVe;
                    lv.LoaiKhachHang = LoaiKhachHang;
                    int n = 0;
                    try
                    {
                        n = db.SaveChanges();
                    }
                    catch
                    {

                    }
                    if (n > 0)
                    {
                        TenLoaiVe = string.Empty;
                        TenKhachHang = string.Empty;
                        LoaiKhachHang = string.Empty;
                        GiaVeLoaiVe = string.Empty;
                        this.CurPage = 1;
                        loadLoaiVe(this.CurPage);
                        MessageBox.Show("Cập nhật thành công");
                    }

                }

                );
        }

        // end TaoVe window

        //  ++++++ begin paged list command
        public ICommand ForwardButtonCommand { get; set; }
        private void ForwardButtonCommandMethod()
        {
            ForwardButtonCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    this.CurPage = 1;
                    if (ManHinhDangChay == 3)
                        loadLoaiVe(this.CurPage);
                    if (ManHinhDangChay == 2)
                        loadSuatChieu(this.CurPage);
                    if (ManHinhDangChay == 1)
                        loadVe(this.CurPage);                    
                }
                );
        }

        public ICommand BackButtonCommand { get; set; }
        private void BackButtonCommandMethod()
        {
            BackButtonCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    if (this.CurPage > 1)
                    {
                        this.CurPage--;
                        if (ManHinhDangChay == 3)
                            loadLoaiVe(this.CurPage);
                        if (ManHinhDangChay == 2)
                            loadSuatChieu(this.CurPage);
                        if (ManHinhDangChay == 1)
                            loadVe(this.CurPage);                       
                    }
                }
                );
        }

        public ICommand NextButtonCommand { get; set; }
        private void NextButtonCommandMethod()
        {
            NextButtonCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    if (this.CurPage < this.TotalPage)
                    {
                        this.CurPage++;
                        if (ManHinhDangChay == 3)
                            loadLoaiVe(this.CurPage);
                        if (ManHinhDangChay == 2)
                            loadSuatChieu(this.CurPage);
                        if (ManHinhDangChay == 1)
                            loadVe(this.CurPage);                       
                    }
                }
                );

        }

        public ICommand LastButtonCommand { get; set; }
        private void LastButtonCommandMethod()
        {
            LastButtonCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    this.CurPage = this.TotalPage;
                    if (ManHinhDangChay == 3)
                        loadLoaiVe(this.CurPage);
                    if (ManHinhDangChay == 2)
                        loadSuatChieu(this.TotalPage);
                    if (ManHinhDangChay == 1)
                        loadVe(this.CurPage);                    
                }
                );

        }

        //  ++++++ end paged list command
        //end command
        ////////////////////////////////////////////////////////////////
        //QLNhaCungCap

        private WindowState _curWindowState;
        public WindowState CurWindowState
        {
            get
            {
                return _curWindowState;
            }
            set
            {
                _curWindowState = value;
                OnPropertyChanged("CurWindowState");
            }
        }
        private ObservableCollection<NhaCCPhim> _listNhaCC;
        public ObservableCollection<NhaCCPhim> listNhaCC
        {
            get { return _listNhaCC; }
            set { _listNhaCC = value; }
        }
        private void loadNhaCC()
        {
            if (ManHinhDangChay != 11 && ManHinhDangChay != 13)
                return;
            var result = (from p in db.NhaCCPhims
                          where p.BiXoa == false
                          select new
                          {
                              p.MaNhaCC,
                              p.TenNhaCC
                          });
            foreach (var r in result)
            {
                NhaCCPhim ncc = new NhaCCPhim()
                {
                    MaNhaCC = r.MaNhaCC,
                    TenNhaCC = r.TenNhaCC
                };
                listNhaCC.Add(ncc);
            }
            //var result = db.NhaCCPhims.Where(q => q.BiXoa == false);
            //TotalPage = (int)Math.Ceiling(result.Count() * 1.0 / PageSize);
            //var result2 = result.OrderByDescending(k => k.TenNhaCC).Skip((curPage - 1) * PageSize).Take(PageSize).ToList();

            //foreach (var r in result2)
            //{
            //    NhaCCPhim lp = new NhaCCPhim()
            //    {
            //        MaNhaCC = r.MaNhaCC,
            //        TenNhaCC = r.TenNhaCC
            //    };
            //    listNhaCC.Add(lp);               
            //}
        }

        public ICommand CloseWindowCommand { get; set; }
        private void CloseWindowCommandMethod()
        {
            CloseWindowCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    ((Window)s).Close();
                });
        }

        public ICommand TimKiemNCCCommand { get; set; }
        private void TimKiemNCCCommandMethod()
        {
            TimKiemNCCCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    if (string.IsNullOrEmpty(s.ToString()))
                    {
                        MessageBox.Show("Xin moi nhap noi dung.");
                        return;
                    }
                    var result = db.NhaCCPhims.Where(k => k.TenNhaCC.Contains(s.ToString().Trim())).Select(k => k).ToList();
                    listNhaCC.Clear();
                    foreach (var item in result)
                    {
                        listNhaCC.Add(item);
                    }
                }
                );
        }

        string _mancc;
        public string ManhaCC
        {
            get { return _mancc; }
            set
            {
                if (value == _mancc)
                {
                    return;
                }
                _mancc = value;
                OnPropertyChanged("MaNhaCC");
            }
        }
        string _tenNcc;
        public string TennhaCC
        {
            get { return _tenNcc; }
            set
            {
                if (value == _tenNcc)
                {
                    return;
                }
                _tenNcc = value;
                OnPropertyChanged("TenNhaCC");
            }
        }

        public ICommand ThemNhaCCCommand { get; set; }
        private void ThemNhaCCCommandMethod()
        {
            ThemNhaCCCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    //if (string.IsNullOrEmpty(ManhaCC))
                    //{
                    //    MessageBox.Show("Nhập mã nhà cung cấp");
                    //    return;
                    //}
                    if (string.IsNullOrEmpty(TennhaCC))
                    {
                        MessageBox.Show("Nhập tên nhà cung cấp");
                        return;
                    }
                    int count = db.NhaCCPhims.Count() + 1;
                    NhaCCPhim ncc = new NhaCCPhim()
                    {
                        MaNhaCC = "NCC00" + count,
                        TenNhaCC = TennhaCC,
                        BiXoa = false
                    };
                    MessageBoxResult conf = MessageBox.Show("Bạn có thực sự muốn thêm.", "", MessageBoxButton.YesNo);
                    if (conf == MessageBoxResult.No)
                    {
                        return;
                    }
                    try
                    {
                        listNhaCC.Add(ncc);
                        db.NhaCCPhims.Add(ncc);
                        db.SaveChanges();
                        listNhaCC.Clear();
                        loadNhaCC();
                        MessageBox.Show("Thêm thành công.");
                        ManhaCC = "";
                        TennhaCC = "";
                    }
                    catch
                    {
                        MessageBox.Show("Thêm thất bại.");
                        return;
                    }
                }
                );
        }

        public ICommand SuaNhaCCLoadCommand { get; set; }
        private void SuaNhaCCLoadCommandMethod()
        {
            SuaNhaCCLoadCommand = new RelayCommand<object>(
                (s) => s != null,
                (s) =>
                {
                    NhaCCPhim sc = s as NhaCCPhim;

                    ManhaCC = sc.MaNhaCC;
                    TennhaCC = sc.TenNhaCC;
                }
                );
        }

        public ICommand SuaNhaCCCM { get; set; }
        public void SuaNhaCCCMMethod()
        {
            SuaNhaCCCM = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    if (string.IsNullOrEmpty(ManhaCC.ToString().Trim()))
                    {
                        return;
                    }
                    try
                    {
                        NhaCCPhim result = db.NhaCCPhims.Where(p => p.MaNhaCC == ManhaCC).Select(p => p).First<NhaCCPhim>();
                        result.TenNhaCC = TennhaCC.Trim();
                        db.SaveChanges();
                        listNhaCC.Clear();
                        loadNhaCC();
                        MessageBox.Show("Cập nhập thành công.");
                        ManhaCC = "";
                        TennhaCC = "";
                    }
                    catch
                    {
                        MessageBox.Show("Cập nhập thất bại.");
                        return;
                    }
                }
                );
        }

        public ICommand XoaNhaCCCM { get; set; }
        public void XoaNhaCCCMMethod()
        {
            XoaNhaCCCM = new RelayCommand<NhaCCPhim>(
                (s) => true,
                (s) =>
                {
                    MessageBoxResult mbResult = MessageBox.Show("Bạn muốn xóa?", "Xóa dòng", MessageBoxButton.YesNo);
                    if (mbResult == MessageBoxResult.No)
                    {
                        return;
                    }
                    var result = db.NhaCCPhims.Where(k => k.BiXoa == false).FirstOrDefault(k => k.MaNhaCC.Equals(s.MaNhaCC));
                    if (result == null)
                    {
                        return;
                    }
                    result.BiXoa = true;
                    int n = 0;
                    try
                    {
                        n = db.SaveChanges();
                        listNhaCC.Clear();
                        loadNhaCC();
                        MessageBox.Show("Đã xóa");
                    }
                    catch
                    {
                        MessageBox.Show("Xóa thất bại.");
                        return;
                    }
                }
                );
        }

        //Quan Ly Loai Phim
        private ObservableCollection<LoaiPhim> _listLoaiPhim;
        public ObservableCollection<LoaiPhim> listLoaiPhim
        {
            get { return _listLoaiPhim; }
            set { _listLoaiPhim = value; }
        }
        private void loadLoaiPhim()
        {
            if (ManHinhDangChay != 10 && ManHinhDangChay != 13)
                return;
            var result = (from p in db.LoaiPhims
                          where p.BiXoa == false
                          select new
                          {
                              p.MaLoai,
                              p.TenTheLoai
                          });
            foreach (var r in result)
            {
                LoaiPhim lp = new LoaiPhim()
                {
                    MaLoai = r.MaLoai,
                    TenTheLoai = r.TenTheLoai
                };
                listLoaiPhim.Add(lp);
            }
            //var result = db.LoaiPhims.Where(q => q.BiXoa == false);
            //TotalPage = (int)Math.Ceiling(result.Count() * 1.0 / PageSize);
            //var result2 = result.OrderByDescending(k => k.TenTheLoai).Skip((curPage - 1) * PageSize).Take(PageSize).ToList();

            //foreach (var r in result2)
            //{
            //    LoaiPhim lp = new LoaiPhim()
            //    {
            //        MaLoai = r.MaLoai,
            //        TenTheLoai = r.TenTheLoai
            //    };
            //    listLoaiPhim.Add(lp);
            //}
        }

        string _maloaip;
        public string Maloaiphim
        {
            get { return _maloaip; }
            set
            {
                if (value == _maloaip)
                {
                    return;
                }
                _maloaip = value;
                OnPropertyChanged("Maloaiphim");
            }
        }
        string _tenLoaiPhim;
        public string Tenloaiphim
        {
            get { return _tenLoaiPhim; }
            set
            {
                if (value == _tenLoaiPhim)
                {
                    return;
                }
                _tenLoaiPhim = value;
                OnPropertyChanged("Tenloaiphim");
            }
        }

        public ICommand TimKiemLoaiPhimCommand { get; set; }
        private void TimKiemLoaiPhimCommandMethod()
        {
            TimKiemLoaiPhimCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    if (string.IsNullOrEmpty(s.ToString()))
                    {
                        MessageBox.Show("Xin moi nhap noi dung.");
                        return;
                    }
                    var result = db.LoaiPhims.Where(k => k.TenTheLoai.Contains(s.ToString().Trim())).Select(k => k).ToList();
                    listLoaiPhim.Clear();
                    foreach (var item in result)
                    {
                        listLoaiPhim.Add(item);
                    }
                }
                );
        }

        public ICommand SuaLoaiPhimLoadCommand { get; set; }
        private void SuaLoaiPhimLoadCommandMethod()
        {
            SuaLoaiPhimLoadCommand = new RelayCommand<object>(
                (s) => s != null,
                (s) =>
                {
                    LoaiPhim lp = s as LoaiPhim;

                    Maloaiphim = lp.MaLoai;
                    Tenloaiphim = lp.TenTheLoai;
                }
                );
        }

        public ICommand ThemLoaiPhimCommand { get; set; }
        private void ThemLoaiPhimCommandMethod()
        {
            ThemLoaiPhimCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    if (string.IsNullOrEmpty(Tenloaiphim))
                    {
                        MessageBox.Show("Nhập tên loại phim.");
                        return;
                    }
                    int count = db.LoaiPhims.Count() + 1;
                    LoaiPhim lp = new LoaiPhim()
                    {
                        MaLoai = "LP00" + count,
                        TenTheLoai = Tenloaiphim,
                        BiXoa = false
                    };
                    MessageBoxResult conf = MessageBox.Show("Bạn có thực sự muốn thêm.", "", MessageBoxButton.YesNo);
                    if (conf == MessageBoxResult.No)
                    {
                        return;
                    }
                    try
                    {
                        listLoaiPhim.Add(lp);
                        db.LoaiPhims.Add(lp);
                        db.SaveChanges();
                        listLoaiPhim.Clear();
                        loadLoaiPhim();
                        MessageBox.Show("Thêm thành công.");
                        Maloaiphim = "";
                        Tenloaiphim = "";
                    }
                    catch
                    {
                        MessageBox.Show("Thêm thất bại.");
                        return;
                    }
                }
                );
        }

        public ICommand SuaLoaiPhimCommand { get; set; }
        public void SuaLoaiPhimCommmandMethod()
        {
            SuaLoaiPhimCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    if (string.IsNullOrEmpty(Maloaiphim.ToString().Trim()))
                    {
                        return;
                    }
                    try
                    {
                        LoaiPhim result = db.LoaiPhims.Where(p => p.MaLoai == Maloaiphim).Select(p => p).First<LoaiPhim>();
                        result.TenTheLoai = Tenloaiphim.Trim();
                        db.SaveChanges();
                        listLoaiPhim.Clear();
                        loadLoaiPhim();
                        MessageBox.Show("Cập nhập thành công.");
                        Maloaiphim = "";
                        Tenloaiphim = "";
                    }
                    catch
                    {
                        MessageBox.Show("Cập nhập thất bại.");
                        return;
                    }
                }
                );
        }

        public ICommand XoaLoaiPhimCommand { get; set; }
        public void XoaLoaiPhimCommandMethod()
        {
            XoaLoaiPhimCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    MessageBoxResult mbResult = MessageBox.Show("Bạn muốn xóa?", "Xóa dòng", MessageBoxButton.YesNo);
                    if (mbResult == MessageBoxResult.No)
                    {
                        return;
                    }
                    var result = db.LoaiPhims.Where(k => k.BiXoa == false).FirstOrDefault(k => k.MaLoai.Equals(k.MaLoai));
                    if (result == null)
                    {
                        return;
                    }
                    result.BiXoa = true;
                    int n = 0;
                    try
                    {
                        n = db.SaveChanges();
                        listLoaiPhim.Clear();
                        loadLoaiPhim();
                        MessageBox.Show("Đã xóa");
                    }
                    catch
                    {
                        MessageBox.Show("Xóa thất bại.");
                        return;
                    }
                }
                );
        }

        //Quan ly phong chieu
        private ObservableCollection<PhongChieu> _listPC;
        public ObservableCollection<PhongChieu> listPC
        {
            get { return _listPC; }
            set { _listPC = value; }
        }
        private void loadPC()
        {
            if (ManHinhDangChay != 12)
                return;
            var result = (from p in db.PhongChieus
                          where p.BiXoa == false
                          select new
                          {
                              p.MaPChieu,
                              p.TenPChieu,
                              p.SoGhe
                          });
            foreach (var r in result)
            {
                PhongChieu lp = new PhongChieu()
                {
                    MaPChieu = r.MaPChieu,
                    TenPChieu = r.TenPChieu,
                    SoGhe = r.SoGhe
                };
                listPC.Add(lp);
            }
        }

        string _mapc;
        public string MaPC
        {
            get { return _mapc; }
            set
            {
                if (value == _mapc)
                {
                    return;
                }
                _mapc = value;
                OnPropertyChanged("MaPC");
            }
        }
        string _tenPC;
        public string TenPC
        {
            get { return _tenPC; }
            set
            {
                if (value == _tenPC)
                {
                    return;
                }
                _tenPC = value;
                OnPropertyChanged("TenPC");
            }
        }

        int? _soghe;
        public int? SoGhe
        {
            get { return _soghe; }
            set
            {
                if (value == _soghe)
                {
                    return;
                }
                _soghe = value;
                OnPropertyChanged("SoGhe");
            }
        }

        public ICommand TimKiemPCCommand { get; set; }
        private void TimKiemPCCommandMethod()
        {
            TimKiemPCCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    if (string.IsNullOrEmpty(s.ToString()))
                    {
                        MessageBox.Show("Xin moi nhap noi dung.");
                        return;
                    }
                    var result = db.PhongChieus.Where(k => k.TenPChieu.Contains(s.ToString().Trim())).Select(k => k).ToList();
                    foreach (var item in result)
                    {
                        listPC.Clear();
                        listPC.Add(item);
                    }
                }
                );
        }

        public ICommand SuaPCLoadCommand { get; set; }
        private void SuaPCLoadCommandMethod()
        {
            SuaPCLoadCommand = new RelayCommand<object>(
                (s) => s != null,
                (s) =>
                {
                    PhongChieu lp = s as PhongChieu;

                    MaPC = lp.MaPChieu;
                    TenPC = lp.TenPChieu;
                    SoGhe = lp.SoGhe;
                }
                );
        }

        public ICommand ThemPCCommand { get; set; }
        private void ThemPCCommandMethod()
        {
            ThemPCCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    int count = db.PhongChieus.Count() + 1;
                    if (string.IsNullOrEmpty(TenPC.ToString().Trim()))
                    {
                        MessageBox.Show("Nhập tên phòng chiếu.");
                        return;
                    }
                    if (string.IsNullOrEmpty(SoGhe.ToString().Trim()))
                    {
                        MessageBox.Show("Nhập số ghế.");
                        return;
                    }

                    PhongChieu lp = new PhongChieu()
                    {
                        MaPChieu = "PC00" + count,
                        TenPChieu = TenPC,
                        SoGhe = SoGhe,
                        BiXoa = false
                    };
                    MessageBoxResult conf = MessageBox.Show("Bạn có thực sự muốn thêm.", "", MessageBoxButton.YesNo);
                    if (conf == MessageBoxResult.No)
                    {
                        return;
                    }
                    try
                    {
                        listPC.Add(lp);
                        db.PhongChieus.Add(lp);
                        db.SaveChanges();
                        listPC.Clear();
                        loadPC();
                        MessageBox.Show("Thêm thành công.");
                        MaPC = "";
                        TenPC = "";
                        SoGhe = null;
                    }
                    catch
                    {
                        MessageBox.Show("Thêm thất bại.");
                        return;
                    }
                }
                );
        }

        public ICommand SuaPCCommand { get; set; }
        public void SuaPCCommmandMethod()
        {
            SuaPCCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    if (string.IsNullOrEmpty(MaPC.ToString().Trim()))
                    {
                        return;
                    }
                    try
                    {
                        PhongChieu result = db.PhongChieus.Where(p => p.MaPChieu == MaPC).Select(p => p).First<PhongChieu>();
                        result.TenPChieu = TenPC.Trim();
                        result.SoGhe = SoGhe;
                        db.SaveChanges();
                        listPC.Clear();
                        loadPC();
                        MessageBox.Show("Cập nhập thành công.");
                        MaPC = "";
                        TenPC = "";
                        SoGhe = null;
                    }
                    catch
                    {
                        MessageBox.Show("Cập nhập thất bại.");
                        return;
                    }
                }
                );
        }

        public ICommand XoaPCCommand { get; set; }
        public void XoaPCCommandMethod()
        {
            XoaPCCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    MessageBoxResult mbResult = MessageBox.Show("Bạn muốn xóa?", "Xóa dòng", MessageBoxButton.YesNo);
                    if (mbResult == MessageBoxResult.No)
                    {
                        return;
                    }
                    var result = db.PhongChieus.Where(k => k.BiXoa == false).FirstOrDefault(k => k.MaPChieu.Equals(k.MaPChieu));
                    if (result == null)
                    {
                        return;
                    }
                    result.BiXoa = true;
                    int n = 0;
                    try
                    {
                        n = db.SaveChanges();
                        listPC.Clear();
                        loadPC();
                        MessageBox.Show("Đã xóa");
                    }
                    catch
                    {
                        MessageBox.Show("Xóa thất bại.");
                        return;
                    }
                }
                );
        }

        //phim
        private ObservableCollection<NhanVien> _listNV;
        public ObservableCollection<NhanVien> listNV
        {
            get { return _listNV; }
            set { _listNV = value; }
        }
        private void loadNV()
        {
            if (ManHinhDangChay != 13)
                return;
            var result = (from s in db.NhanViens.Include("NhanVien")
                          where s.BiXoa == false
                          select new
                          {
                              s.MaNV,
                              s.TenNV
                          });
            foreach (var r in result)
            {
                NhanVien lp = new NhanVien()
                {
                    MaNV = r.MaNV,
                    TenNV = r.TenNV
                };
                listNV.Add(lp);
            }
        }

        private ObservableCollection<Phim> _listDSPhim;
        public ObservableCollection<Phim> listDSPhim
        {
            get { return _listDSPhim; }
            set { _listDSPhim = value; }
        }
        private void loadDSPhim()
        {
            if (ManHinhDangChay != 13)
                return;
            var result = (from p in db.Phims
                          where p.BiXoa == false
                          select new
                          {
                              p.MaPhim,
                              p.TenPhim,
                              p.LoaiPhim,
                              p.NhaCCPhim,
                              p.NhanVien,
                              p.MieuTa,
                              p.PosterFilm
                          });
            foreach (var r in result)
            {
                Phim lp = new Phim()
                {
                    MaPhim = r.MaPhim,
                    TenPhim = r.TenPhim,
                    NhaCCPhim = r.NhaCCPhim,
                    LoaiPhim = r.LoaiPhim,
                    NhanVien = r.NhanVien,
                    MieuTa = r.MieuTa,
                    PosterFilm = r.PosterFilm
                };
                listDSPhim.Add(lp);
            }
            //var result = db.Phims.Where(q => q.BiXoa == false);
            //TotalPage = (int)Math.Ceiling(result.Count() * 1.0 / PageSize);
            //var result2 = result.OrderByDescending(k => k.TenPhim).Skip((curPage - 1) * PageSize).Take(PageSize).ToList();

            //foreach (var r in result2)
            //{
            //    Phim lp = new Phim()
            //    {
            //        MaPhim = r.MaPhim,
            //        TenPhim = r.TenPhim,
            //        NhaCCPhim = r.NhaCCPhim,
            //        LoaiPhim = r.LoaiPhim,
            //        NhanVien = r.NhanVien,
            //        MieuTa = r.MieuTa,
            //        PosterFilm = r.PosterFilm
            //    };
            //    listDSPhim.Add(lp);
            //}
        }

        string flie;
        public string imagePhoto
        {
            get { return flie; }
            set
            {
                if (value == flie)
                {
                    return;
                }
                flie = value;
                OnPropertyChanged("imagePhoto");
            }
        }

        string _map;
        public string MaPhim
        {
            get { return _map; }
            set
            {
                if (value == _map)
                {
                    return;
                }
                _map = value;
                OnPropertyChanged("MaPhim");
            }
        }
        string _tenphim;
        public string TenPhim
        {
            get { return _tenphim; }
            set
            {
                if (value == _tenphim)
                {
                    return;
                }
                _tenphim = value;
                OnPropertyChanged("TenPhim");
            }
        }

        string _nvql;
        public string NVQL
        {
            get { return _nvql; }
            set
            {
                if (value == _nvql)
                {
                    return;
                }
                _nvql = value;
                OnPropertyChanged("NVQL");
            }
        }

        string _mieuta;
        public string Mieuta
        {
            get { return _mieuta; }
            set
            {
                if (value == _mieuta)
                {
                    return;
                }
                _mieuta = value;
                OnPropertyChanged("Mieuta");
            }
        }

        public ICommand ChonHinhCommand { get; set; }
        public void ChonHinhCommandMethod()
        {
            ChonHinhCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    OpenFileDialog op = new OpenFileDialog();
                    op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                                "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                                "Portable Network Graphic (*.png)|*.png";
                    Nullable<bool> result = op.ShowDialog();

                    if (result == true)
                    {
                        imagePhoto = op.FileName.ToString();

                    }
                }
                );
        }

        public ICommand TimKiemPhimCommand { get; set; }
        private void TimKiemPhimCommandMethod()
        {
            TimKiemPhimCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    if (string.IsNullOrEmpty(s.ToString()))
                    {
                        MessageBox.Show("Xin moi nhap noi dung.");
                        return;
                    }
                    var result = db.Phims.Where(k => k.TenPhim.Contains(s.ToString().Trim())).Select(k => k).ToList();
                    foreach (var item in result)
                    {
                        listDSPhim.Clear();
                        listDSPhim.Add(item);
                    }
                }
                );
        }

        public ICommand PhimLoadCommand { get; set; }
        private void PhimLoadCommandMethod()
        {
            PhimLoadCommand = new RelayCommand<object>(
                (s) => s != null,
                (s) =>
                {
                    Phim lp = s as Phim;
                    MaPhim = lp.MaPhim;
                    TenPhim = lp.TenPhim;
                    Maloaiphim = lp.LoaiPhim.MaLoai;
                    ManhaCC = lp.NhaCCPhim.MaNhaCC;
                    NVQL = lp.NhanVien.MaNV;
                    Mieuta = lp.MieuTa;
                    imagePhoto = lp.PosterFilm;
                }
                );
        }

        public ICommand ThemPhimCommand { get; set; }
        private void ThemPhimCommandMethod()
        {

            ThemPhimCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    if (string.IsNullOrEmpty(TenPhim))
                    {
                        MessageBox.Show("Nhập tên phim.");
                        return;
                    }
                    if (string.IsNullOrEmpty(imagePhoto))
                    {
                        MessageBox.Show("Chọn poster.");
                        return;
                    }

                    string[] linkAnh = imagePhoto.Split('\\');
                    string poster = linkAnh[linkAnh.Count() - 1];
                    int count = db.Phims.Count() + 1;
                    Phim p = new Phim()
                    {
                        MaPhim = "P00" + count,
                        TenPhim = TenPhim.Trim(),
                        TheLoai = Maloaiphim,
                        NhaCungCap = ManhaCC,
                        NhanVienQL = NVQL,
                        MieuTa = Mieuta.Trim(),
                        PosterFilm = poster.Trim(),
                        BiXoa = false
                    };
                    MessageBoxResult conf = MessageBox.Show("Bạn có thực sự muốn thêm.", "", MessageBoxButton.YesNo);
                    if (conf == MessageBoxResult.No)
                    {
                        return;
                    }
                    try
                    {
                        string str = "Auxiliary\\Resouce\\Image";
                        string destinationPath = str.GetDes() + p.PosterFilm;

                        File.Copy(imagePhoto, destinationPath, true);

                        db.Phims.Add(p);
                        db.SaveChanges();
                        MessageBox.Show("Thêm thành công.");
                        listDSPhim.Clear();
                        loadDSPhim();
                    }
                    catch
                    {
                        MessageBox.Show("Thêm thất bại.");
                        return;
                    }
                }
                );
        }

        public ICommand XoaPhimCommand { get; set; }
        private void XoaPhimCommandMethod()
        {
            XoaPhimCommand = new RelayCommand<Phim>(
                (s) => s != null,
                (s) =>
                {
                    if (s == null)
                    {
                        return;
                    }
                    MessageBoxResult conf = MessageBox.Show("Bạn có thực sự muốn xóa.", "", MessageBoxButton.YesNo);
                    if (conf == MessageBoxResult.No)
                    {
                        return;
                    }
                    var p = db.Phims.Where(k => k.BiXoa == false).FirstOrDefault(k => k.MaPhim.Equals(s.MaPhim));
                    if (p == null)
                    {
                        return;
                    }
                    p.BiXoa = true;
                    int n = 0;
                    try
                    {
                        n = db.SaveChanges();
                    }
                    catch
                    {

                    }
                    if (n > 0)
                    {
                        string str = "Auxiliary\\Resouce\\Image";
                        string destinationPath = str.GetDes() + s.PosterFilm;
                        if (System.IO.File.Exists(destinationPath))
                        {
                            System.IO.File.Delete(destinationPath);
                        }
                        listDSPhim.Remove(s);
                    }
                }
                );
        }

        public ICommand SuaPhimCommand { get; set; }
        public void SuaPhimCommandMethod()
        {
            SuaPhimCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    if (string.IsNullOrEmpty(MaPhim))
                    {
                        return;
                    }

                    if (MaPhim == null)
                        return;
                    string posterFilm = "";
                    Phim result = db.Phims.Where(p => p.MaPhim.Equals(MaPhim.Trim())).Select(p => p).First<Phim>();
                    result.TenPhim = TenPhim;
                    result.TheLoai = Maloaiphim;
                    result.NhaCungCap = ManhaCC;
                    result.NhanVienQL = NVQL;
                    result.MieuTa = Mieuta;

                    posterFilm = result.PosterFilm;

                    string[] linkAnh = imagePhoto.Split('\\');
                    string poster = linkAnh[linkAnh.Count() - 1];
                    if (!posterFilm.Equals(poster))
                    {
                        result.PosterFilm = poster;
                        string str = "Auxiliary\\Resouce\\Image";
                        string destinationPath = str.GetDes() + poster;
                        File.Copy(imagePhoto, destinationPath, true);

                    }
                    int n = 0;
                    try
                    {
                        n = db.SaveChanges();
                    }
                    catch
                    {
                        MessageBox.Show("Cập nhập thất bại.");
                        return;
                    }

                    if (n > 0)
                    {
                        string str = "Auxiliary\\Resouce\\Image";
                        string destinationPathXoa = str.GetDes() + posterFilm;
                        if (System.IO.File.Exists(destinationPathXoa))
                        {
                            System.IO.File.Delete(destinationPathXoa);
                        }
                        listDSPhim.Clear();
                        loadDSPhim();
                        MessageBox.Show("Cập nhập thành công.");
                    }
                }
                );
        }

        //public static String GetDes(string foldername)
        //{
        //    //String app = System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
        //    string app = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));

        //    string des = String.Format(app + "\\{0}\\", foldername);
        //    return des;
        //}

        ////THI///////////////////////////////////////////////////////////////
        private ObservableCollection<NhanVien> _listNhanVien;
        public ObservableCollection<NhanVien> listNhanVien
        {
            get { return _listNhanVien; }
            set { _listNhanVien = value; }
        }
        private void loadNhanVien()
        {
            if (ManHinhDangChay != 4)
            {
                return;
            }
            var nv = (from p in db.NhanViens
                      where p.BiXoa == false && p.TenNV != "Admin"
                      select new
                      {
                          p.MaNV,
                          p.TenNV,
                          p.LoaiNV.VaiTro,
                          p.SDTNV,
                          p.DiaChi
                      }).ToList();
            foreach (var item in nv)
            {
                LoaiNV lnv = new LoaiNV()
                {
                    VaiTro = item.VaiTro
                };

                NhanVien nvs = new NhanVien()
                {
                    MaNV = item.MaNV,
                    TenNV = item.TenNV,
                    LoaiNV = lnv,
                    SDTNV = item.SDTNV,
                    DiaChi = item.DiaChi
                };
                listNhanVien.Add(nvs);
            }
        }

        private ObservableCollection<TaiKhoan> _listTaiKhoan;
        public ObservableCollection<TaiKhoan> listTaiKhoan
        {
            get { return _listTaiKhoan; }
            set { _listTaiKhoan = value; }
        }
        private void loadTaiKhoan()
        {
            if (ManHinhDangChay != 4)
            {
                return;
            }

            var tk = (from p in db.TaiKhoans
                      where p.BiXoa == false && p.TenDangNhap != "Admin               "
                      select new
                      {
                          p.MaTaiKhoan,
                          p.TenDangNhap,
                          p.MatKhau,
                          p.MaNV,
                          p.LoaiTaiKhoan.TenLoaiTaiKhoan,
                          p.NhanVien.TenNV
                      }).ToList();
            foreach (var item in tk)
            {
                LoaiTaiKhoan ltk = new LoaiTaiKhoan()
                {
                    TenLoaiTaiKhoan = item.TenLoaiTaiKhoan
                };

                NhanVien nv = new NhanVien()
                {
                    TenNV = item.TenNV
                };

                TaiKhoan tks = new TaiKhoan()
                {
                    MaTaiKhoan = item.MaTaiKhoan,
                    TenDangNhap = item.TenDangNhap,
                    MatKhau = item.MatKhau,
                    MaNV = item.MaNV,
                    LoaiTaiKhoan = ltk,
                    NhanVien = nv
                };
                listTaiKhoan.Add(tks);
            }
        }

        private ObservableCollection<LoaiNV> _listChucVu;
        public ObservableCollection<LoaiNV> listChucVu
        {
            get { return _listChucVu; }
            set { _listChucVu = value; }
        }
        private void loadChucVu()
        {
            int a = ManHinhDangChay;
            if (ManHinhDangChay != 4)
            {
                return;
            }
            var chucVu = db.LoaiNVs.ToList();
            foreach (var cv in chucVu)
            {
                listChucVu.Add(cv);
            }
        }

        private ObservableCollection<LoaiTaiKhoan> _listLoaiTaiKhoan;
        public ObservableCollection<LoaiTaiKhoan> listLoaiTaiKhoan
        {
            get { return _listLoaiTaiKhoan; }
            set { _listLoaiTaiKhoan = value; }
        }
        private void loadLoaiTaiKhoan()
        {
            if (ManHinhDangChay != 4)
            {
                return;
            }

            var loaiTK = db.LoaiTaiKhoans.ToList();
            foreach (var ltk in loaiTK)
            {
                listLoaiTaiKhoan.Add(ltk);
            }
        }

        private ObservableCollection<KhachHang> _listKhachHang;
        public ObservableCollection<KhachHang> listKhachHang
        {
            get { return _listKhachHang; }
            set { _listKhachHang = value; }
        }
        private void loadKhachHang()
        {
            if (ManHinhDangChay != 4)
            {
                return;
            }

            var kh = (from k in db.KhachHangs
                      where k.BiXoa == false
                      select new
                      {
                          k.SDT,
                          k.TongTien
                      }).ToList();
            foreach (var item in kh)
            {
                KhachHang khs = new KhachHang()
                {
                    SDT = item.SDT,
                    TongTien = item.TongTien
                };
                listKhachHang.Add(khs);
            }
        }
        //Xóa, Cập nhật, Tìm kiếm khách hàng
        public ICommand XoaKhachHangCommand { get; set; }
        private void XoaKhachHangCommandMethod()
        {
            XoaKhachHangCommand = new RelayCommand<object>(
                (s) => s != null,
                (s) =>
                {
                    KhachHang kh = s as KhachHang;
                    var result = db.KhachHangs.Where(k => k.SDT == kh.SDT).Select(k => k).ToList();
                    foreach (var r in result)
                    {
                        try
                        {
                            r.BiXoa = true;
                            db.SaveChanges();
                            listKhachHang.Remove(kh);
                            MessageBox.Show("Xóa khách hàng thành công");
                        }
                        catch
                        {
                        }
                        break;
                    }
                }
                );
        }
        //Cập nhật khách hàng.
        //string _sdt;
        //public string SDT
        //{
        //    get { return _sdt; }
        //    set
        //    {
        //        if (value == _sdt)
        //        {
        //            return;
        //        }
        //        _sdt = value;
        //        OnPropertyChanged("SDT");
        //    }
        //}
        //public ICommand CapNhatKhachHangCommand { get; set; }
        //private void CapNhatKhachHangCommandMethod()
        //{
        //    CapNhatKhachHangCommand = new RelayCommand<object>(
        //        (s) => s != null,
        //        (s) =>
        //        {
        //            KhachHang kh = s as KhachHang;
        //            var result = db.KhachHangs.Where(k => k.SDT == kh.SDT).Select(k => k).ToList();
        //            foreach (var r in result)
        //            {
        //                try
        //                {

        //                    r.SDT = SDT;
        //                    db.SaveChanges();
        //                    listKhachHang.Clear();
        //                    loadKhachHang();
        //                    MessageBox.Show("Cập nhật khách hàng thành công");
        //                }
        //                catch
        //                {
        //                    MessageBox.Show("Loi cap nhat");
        //                }
        //                break;
        //            }
        //        }
        //        );
        //}
        string _tkSDT;
        public string TKSDT
        {
            get { return _tkSDT; }
            set
            {
                if (value == _tkSDT)
                {
                    return;
                }
                _tkSDT = value;
                OnPropertyChanged("TKSDT");
            }
        }
        public ICommand TimKiemKhachHangCommand { get; set; }
        private void TimKiemKhachHangCommandMethod()
        {
            TimKiemKhachHangCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    //if (string.IsNullOrEmpty(TKSDT.Trim()))
                    //{
                    //    MessageBox.Show("Chưa nhập thừ khóa");
                    //    return;
                    //}
                    try
                    {


                        var result = db.KhachHangs.Where(k => k.SDT.Contains(TKSDT) && k.BiXoa == false).Select(k => k).ToList();
                        listKhachHang.Clear();
                        foreach (var item in result)
                        {
                            listKhachHang.Add(item);
                        }
                    }
                    catch
                    {

                    }

                }
                );
        }
        public ICommand LamMoiKhachHangCommand { get; set; }
        private void LamMoiKhachHangCommandMethod()
        {
            LamMoiKhachHangCommand = new RelayCommand<object>(
                (s) => true,
                (s) =>
                {
                    listKhachHang.Clear();
                    var kh = (from k in db.KhachHangs
                              where k.BiXoa == false
                              select new
                              {
                                  k.SDT,
                                  k.TongTien
                              }).ToList();
                    foreach (var item in kh)
                    {
                        KhachHang khs = new KhachHang()
                        {
                            SDT = item.SDT,
                            TongTien = item.TongTien
                        };
                        listKhachHang.Add(khs);
                    }
                }
                );
        }
        /////// /////////////////////////////////////////////////////////////////////////////

        //Thêm, Xóa, Sửa Nhân Viên

        string _maNhanVien;
        public string MaNhanVien
        {
            get { return _maNhanVien; }
            set
            {
                if (value == _maNhanVien)
                {
                    return;
                }
                _maNhanVien = value;
                OnPropertyChanged("MaNhanVien");
            }
        }
        string _tenNhanVien;
        public string TenNhanVien
        {
            get { return _tenNhanVien; }
            set
            {
                if (value == _tenNhanVien)
                {
                    return;
                }
                _tenNhanVien = value;
                OnPropertyChanged("TenNhanVien");
            }
        }
        object _chucVu;
        public object ChucVu
        {
            get { return _chucVu; }
            set
            {
                if (_chucVu == value)
                    return;
                _chucVu = value;
                OnPropertyChanged("ChucVu");
            }
        }

        string _sdtnv;
        public string SDTNV
        {
            get { return _sdtnv; }
            set
            {
                if (value == _sdtnv)
                {
                    return;
                }
                _sdtnv = value;
                OnPropertyChanged("SDTNV");
            }
        }

        string _diachiNV;
        public string DiaChi
        {
            get { return _diachiNV; }
            set
            {
                if (value == _diachiNV)
                {
                    return;
                }
                _diachiNV = value;
                OnPropertyChanged("DiaChi");
            }
        }

        public ICommand TaoNhanVienCommand { get; set; }
        private void TaoNhanVienCommandMethod()
        {
            TaoNhanVienCommand = new RelayCommand<object>(
                (s) =>
                {
                    if (TenNhanVien == null)
                        return false;
                    if (ChucVu == null)
                        return false;
                    if (SDTNV == null)
                        return false;
                    if (DiaChi == null)
                        return false;
                    return true;

                },
                (s) =>
                {
                    string chucvu = ChucVu.ToString();
                    if (TenNhanVien == "" || SDTNV == "" || DiaChi == "")
                    {
                        MessageBox.Show("Không được để trống.");
                        return;
                    }
                    int dodai = SDTNV.Length;
                    Regex regex = new Regex(@"^[0-9]+$");
                    if (!regex.IsMatch(SDTNV.Trim()) || dodai > 11)
                    {
                        MessageBox.Show("Số điện thoại không hợp lệ");
                        return;
                    }
                    int stt = db.NhanViens.Count();
                    stt++;
                    MaNhanVien = "NV0" + stt;
                    NhanVien nv = new NhanVien
                    {
                        MaNV = MaNhanVien,
                        TenNV = TenNhanVien,
                        MaLoaiNV = chucvu,
                        SDTNV = SDTNV,
                        DiaChi = DiaChi,
                        BiXoa = false
                    };

                    db.NhanViens.Add(nv);
                    int n = 0;
                    try
                    {
                        n = db.SaveChanges();
                    }
                    catch
                    {
                        MessageBox.Show("Mời bạn chọn chức vụ cho nhân viên");
                    }
                    if (n > 0)
                    {
                        listNhanVien.Add(nv);
                        TenNhanVien = string.Empty;
                        ChucVu = null;
                        SDTNV = string.Empty;
                        DiaChi = string.Empty;
                        MessageBox.Show("Thêm nhân viên thành công!");
                    }
                }
                );
        }
        public ICommand XoaNhanVienCommand { get; set; }
        private void XoaNhanVienCommandMethod()
        {
            XoaNhanVienCommand = new RelayCommand<object>(
                (s) => s != null,
                (s) =>
                {
                    NhanVien nv = s as NhanVien;
                    var result = db.NhanViens.Where(k => k.MaNV == nv.MaNV).Select(k => k).ToList();
                    foreach (var r in result)
                    {
                        try
                        {
                            r.BiXoa = true;
                            db.SaveChanges();
                            MessageBox.Show("Nhân viên đã được xóa!");
                            listNhanVien.Remove(nv);
                        }
                        catch
                        {
                        }
                        break;
                    }
                }
                );
        }
        public ICommand CapNhatNhanVienCommand { get; set; }
        private void CapNhatNhanVienCommandMethod()
        {
            CapNhatNhanVienCommand = new RelayCommand<object>(
                (s) => s != null,
                (s) =>
                {
                    string chucvu = ChucVu.ToString();
                    NhanVien nv = s as NhanVien;
                    var result = db.NhanViens.Where(k => k.MaNV == nv.MaNV).Select(k => k).ToList();
                    foreach (var r in result)
                    {
                        try
                        {

                            r.TenNV = TenNhanVien;
                            r.MaLoaiNV = chucvu;
                            r.BiXoa = false;
                            db.SaveChanges();
                            MessageBox.Show("Cập nhật nhân viên thành công!");
                            listNhanVien.Clear();
                        }
                        catch
                        {
                            loadNhanVien();
                        }
                        break;
                    }

                }
                );
        }
        /////////////////////////////////////
        //Thêm, Xóa, Cập Nhật Tài Khoảng
        //Mã hóa md5
        public static byte[] encryptData(string data)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider md5Hasher = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] hashedBytes;
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            hashedBytes = md5Hasher.ComputeHash(encoder.GetBytes(data));
            return hashedBytes;
        }
        public static string md5(string data)
        {
            return BitConverter.ToString(encryptData(data)).Replace("-", "").ToLower();
        }
        //////////////////////////////////
        string _maTaiKhoan;
        public string MaTaiKhoan
        {
            get { return _maTaiKhoan; }
            set
            {
                if (value == _maTaiKhoan)
                {
                    return;
                }
                _maTaiKhoan = value;
                OnPropertyChanged("MaTaiKhoan");
            }
        }
        string _tenDangNhap;
        public string TenDangNhap
        {
            get { return _tenDangNhap; }
            set
            {
                if (value == _tenDangNhap)
                {
                    return;
                }
                _tenDangNhap = value;
                OnPropertyChanged("TenDangNhap");
            }
        }
        object _loaiTaiKhoan;
        public object LoaiTaiKhoan
        {
            get { return _loaiTaiKhoan; }
            set
            {
                if (_loaiTaiKhoan == value)
                    return;
                _loaiTaiKhoan = value;
                OnPropertyChanged("LoaiTaiKhoan");
            }
        }
        string _matKhau;
        public string MatKhau
        {
            get { return _matKhau; }
            set
            {
                if (value == _matKhau)
                {
                    return;
                }
                _matKhau = value;
                OnPropertyChanged("MatKhau");
            }
        }
        string _maNhanVien2;
        public string MaNhanVien2
        {
            get { return _maNhanVien2; }
            set
            {
                if (value == _maNhanVien2)
                {
                    return;
                }
                _maNhanVien2 = value;
                OnPropertyChanged("MaNhanVien2");
            }
        }
        public ICommand TaoTaiKhoanCommand { get; set; }
        private void TaoTaiKhoanCommandMethod()
        {
            TaoTaiKhoanCommand = new RelayCommand<object>(
                (s) =>
                {
                    if (LoaiTaiKhoan == null)
                        return false;
                    if (TenDangNhap == null)
                        return false;
                    if (MatKhau == null)
                        return false;
                    if (MaNhanVien2 == null)
                        return false;
                    return true;

                },
                (s) =>
                {
                    string loaitk = LoaiTaiKhoan.ToString();
                    if (TenDangNhap == "" || MatKhau == "" || MaNhanVien2 == "")
                    {
                        return;
                    }
                    int te = db.TaiKhoans.Where(m => m.TenDangNhap == TenDangNhap).Count();
                    if (te > 0)
                    {
                        MessageBox.Show("Tên đăng nhập đã tồn tại!");
                        return;
                    }
                    int stt = db.TaiKhoans.Count();
                    stt++;
                    MaTaiKhoan = "TK0" + stt;
                    string md5str = MatKhau;
                    string mk = md5(md5str);
                    TaiKhoan tk = new TaiKhoan
                    {
                        MaTaiKhoan = MaTaiKhoan,
                        TenDangNhap = TenDangNhap,
                        MatKhau = mk,
                        BiXoa = false,
                        MaLoaiTaiKhoan = loaitk,
                        MaNV = MaNhanVien2
                    };
                    db.TaiKhoans.Add(tk);
                    int n = 0;
                    try
                    {
                        n = db.SaveChanges();
                    }
                    catch
                    {
                        MessageBox.Show("Mã nhân viên không đúng!");
                    }
                    if (n > 0)
                    {
                        listTaiKhoan.Clear();
                        loadTaiKhoan();
                        TenDangNhap = string.Empty;
                        LoaiTaiKhoan = null;
                        MatKhau = string.Empty;
                        MaNhanVien2 = string.Empty;
                        MessageBox.Show("Tài khoản đã được tạo!");
                    }
                }
                );
        }
        public ICommand XoaTaiKhoanCommand { get; set; }
        private void XoaTaiKhoanCommandMethod()
        {
            XoaTaiKhoanCommand = new RelayCommand<object>(
                (s) => s != null,
                (s) =>
                {
                    TaiKhoan tk = s as TaiKhoan;
                    var result = db.TaiKhoans.Where(k => k.MaTaiKhoan == tk.MaTaiKhoan).Select(k => k).ToList();
                    foreach (var r in result)
                    {
                        try
                        {
                            r.BiXoa = true;
                            db.SaveChanges();
                            MessageBox.Show("Tài khoản đã được xóa!");
                            listTaiKhoan.Remove(tk);
                        }
                        catch
                        {
                        }
                        break;
                    }
                }
                );
        }
        public ICommand CapNhatTaiKhoanCommand { get; set; }
        private void CapNhatTaiKhoanCommandMethod()
        {
            CapNhatTaiKhoanCommand = new RelayCommand<object>(
                (s) => s != null,
                (s) =>
                {
					if (TenDangNhap == "" || MatKhau == "" || MaNhanVien2 == "")
                    {
						MessageBox.Show("Dữ liệu không đầy đủ!");
                        return;
                    }
                    string loaitk = LoaiTaiKhoan.ToString();
                    TaiKhoan tk = s as TaiKhoan;
                    string md5str = MatKhau;
                    string mk = md5(md5str);
                    var result = db.TaiKhoans.Where(k => k.MaTaiKhoan == tk.MaTaiKhoan).Select(k => k).ToList();
                    foreach (var r in result)
                    {
                        try
                        {
                            r.TenDangNhap = TenDangNhap;
                            r.MatKhau = mk;
                            r.MaLoaiTaiKhoan = loaitk;
                            r.MaNV = MaNhanVien2;
                            //r.BiXoa = false;
                            db.SaveChanges();
                            MessageBox.Show("Tài khoản đã dược cập nhật!");
                            listTaiKhoan.Clear();
                        }
                        catch
                        {
                            loadTaiKhoan();
                        }
                        break;
                    }
                }
                );
        }

        ObservableCollection<int> _dsNam;
        public ObservableCollection<int> DSNam
        {
            get { return _dsNam; }
            set
            {
                if (value == _dsNam)
                    return;
                _dsNam = value;
                OnPropertyChanged("DSNam");
            }
        }
        private void loadNam()
        {
            var sc = db.SuatChieus.Where(s => s.BiXoa == false).Select(s => s.Ngay.Year.ToString()).ToList().Distinct();
            var count = sc.Count();
            foreach (var i in sc)
            {
                //DateTime nam = DateTime.Parse(i.ToShortDateString());
                //string n = nam.Year.ToString();
                DSNam.Add(Convert.ToInt32(i));

            }
        }


        /// <summary>
        /// ////////////////////////////////////////////////////////////////
        /// </summary>
    }
}
