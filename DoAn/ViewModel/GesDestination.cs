﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public static class GesDestination
    {
        public static String GetDes(this string foldername)
        {
            //String app = System.IO.Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            string app = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));

            string des = String.Format(app + "\\{0}\\", foldername);
            return des;
        }
    }
}
